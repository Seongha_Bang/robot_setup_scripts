#!/bin/bash

DIR="$(dirname "$(realpath -s "${BASH_SOURCE}")")"


if [[ -f "/home/${USER}/.xsessionrc" ]]
then  # When use xsessionrc

  # Add script on xsessionrc when not exist
  if [[ -z "$(cat /home/${USER}/.xsessionrc | grep -Po "xinput map-to-output")" ]]
  then
    tail -n +2 "${DIR}/rotation_on_xsessionrc.sh" >> /home/${USER}/.xsessionrc
  fi

else  # When use *.desktop file

  # Create autostart folder when not exist
  if [[ ! -d "${HOME}/.config/autostart" ]]
  then
    mkdir ${HOME}/.config/autostart
  fi

  # Create desktop file to autostart
  echo "[Desktop Entry]

Encoding=UTF-8
Version=1.0
Type=Application
Terminal=false
X-GNOME-Autostart-enabled=true

Exec=/bin/bash ${DIR}/rotation_on_xsessionrc.sh
Path=${DIR}

Name=TouchScreenRotation.desktop" | tee ${HOME}/.config/autostart/TouchScreenRotation.desktop

fi
