#!/bin/bash

DIR=$(dirname $(realpath -s ${BASH_SOURCE}))
DIRECTIONS=(Normal Inverted Left Right)

cd ${DIR}


chmod +x ${DIR}/rotation.sh

for direction in "${DIRECTIONS[@]}"
do

echo "[Desktop Entry]

Encoding=UTF-8
Version=1.0
Type=Application
Terminal=false

Exec=${DIR}/rotation.sh ${direction,,}
Path=${DIR}

Name[ko]=TouchScreen_${direction}.desktop" > TouchScreen_${direction}.desktop
chmod +x TouchScreen_${direction}.desktop

done
