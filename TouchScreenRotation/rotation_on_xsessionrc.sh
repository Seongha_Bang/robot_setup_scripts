#!/bin/bash

bash -c "
  while true
  do
    touchs=(\$(xinput list | grep -Poi \"touch[^\n]+id=\d+\" | grep -Po \"(?<=id=)\d+\"))

    for id in \${touchs[@]}
    do
        xinput map-to-output \${id} \$(xrandr | grep primary | grep -Po '^[^\s]+')
    done

    sleep 5
  done
" &
disown ${!}
