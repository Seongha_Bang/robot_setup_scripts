#!/bin/bash

in_array() {
  local needle array value

  needle="${1}"
  shift
  array=("${@}")

  for value in ${array[@]}
  do
    [ "${value}" == "${needle}" ] && echo "${value}" && break
  done
}

# Usage: delete_line [text] [file]
delete_line() {
  while [[ true ]]
  do
    linenum="$(grep -n "${1}" ${2} | head -n 1 | cut -d: -f1)"

    if [[ -n ${linenum} ]]
    then
      sed -i "${linenum}d" ${2}
    else
      break
    fi
  done
}


if [[ -n $(in_array "$1" normal inverted left right) ]]
then
  TRANSFORM="Coordinate Transformation Matrix"
  XSESSIONRC="/home/$(whoami)/.xsessionrc"

  screen_list=($(xrandr | grep -Po "[^\s]+\sconnected" | grep -Po "^[^\s]+"))

  IFS_OLD=${IFS}
  IFS=$'\n'

  touch_list=($(xinput list | grep -Poi "touch[^\n]+id=\d+" | grep -Po "id=\d+" | grep -Po "\d+"))

  IFS=${IFS_OLD}

  delete_line "DISPLAY" "${XSESSIONRC}"
  delete_line "^xrandr" "${XSESSIONRC}"


  for scr in "${screen_list[@]}"
  do
    command="DISPLAY=:0
xrandr --output ${scr} --rotate $1"

    bash -c "${command}"
    echo "${command}">>${XSESSIONRC}
  done

  for touch in "${touch_list[@]}"
  do
    case "$1" in
      normal)
        command="xinput set-prop \"$touch\" \"$TRANSFORM\" 1 0 0 0 1 0 0 0 1"
        ;;
      inverted)
        command="xinput set-prop \"$touch\" \"$TRANSFORM\" -1 0 1 0 -1 1 0 0 1"
        ;;
      left)
        command="xinput set-prop \"$touch\" \"$TRANSFORM\" 0 -1 1 1 0 0 0 0 1"
        ;;
      right)
        command="xinput set-prop \"$touch\" \"$TRANSFORM\" 0 1 0 -1 0 1 0 0 1"
        ;;
    esac

    echo ${command}
    bash -c "${command}"
  done
else
  echo "[Usage] \$1: normal, inverted, left, right"
fi

