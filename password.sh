#! /bin/bash

DIR="$(dirname "$(realpath -s "${BASH_SOURCE}")")"


if [[ -n "${1}" ]]
then
  password=${1}
  retype=${1}
else
  echo -n "Enter new password: "
  read -s -r password
  echo

  echo -n "Retype new password: "
  read -s -r retype
  echo
fi


if [[ "${password}" == "${retype}" ]]
then
  echo "${USER}:${password}" | sudo chpasswd
  
  if [[ -f /etc/hostapd/hostapd.conf ]]
  then
    bash ${DIR}/ap-password.sh ${password} >/dev/null
  fi
  
  if [[ "$(dpkg --print-architecture)" == "amd64" ]]
  then
    gsettings set org.gnome.Vino vnc-password keyring
  else
    bash ${DIR}/vino-vnc-password.sh ${password} >/dev/null
  fi

  echo "Success"
else
  echo "Failed"
fi
