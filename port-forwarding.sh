#!/bin/bash

# < Note >
# 
# This script applies port forwarding
# when the Drv(drive) PC is used as a gateway.
# 
# (Another PC's gateway must be set to Drv PC.)


# < Parameters >

TABLE=(
  # < TABLE Usage >
  # '[I-interface]/[I-port]/[O-ip]/[O-port]'
  #  (I: input, O: output)

  'wlp3s0/8078/192.168.42.78/8078'  # Srv: SSH
  'wlp3s0/5900/192.168.42.78/5900'  # Srv: VNC
  'wlp3s0/8079/192.168.42.79/8079'  # AI:  SSH
  'wlp3s0/5901/192.168.42.79/5901'  # AI:  VNC
  'wlp3s0/1883/192.168.42.78/1883'  # Srv: MQTT (Planner)
)


# < Code >

# Check root permission
if [ "${EUID}" -eq 0 ]
then
  SUDO=""
else
  SUDO="sudo"
fi

# Add routing for port forwarding
for line in ${TABLE[@]}
do
  IFS='/' read -r -a args <<< "${line}"
  for p in tcp udp
  do
    ${SUDO} iptables              \
      -t nat              \
      -A PREROUTING       \
      -p ${p}             \
      -i ${args[0]}       \
      --dport ${args[1]}  \
      -j DNAT             \
      --to-destination ${args[2]}:${args[3]}
  done
done


# Create and enable service to apply at boot
if [[ ! -f /etc/systemd/system/dogu-port-forwarding.service ]]
then
echo "[Unit]
Description=Apply port forwarding
After=network-online.target

[Service]
ExecStart=/bin/bash ${HOME}/.dogu_system/robot_setup_scripts/port-forwarding.sh

[Install]
WantedBy=multi-user.target" | ${SUDO} tee /etc/systemd/system/dogu-port-forwarding.service
fi
${SUDO} systemctl enable dogu-port-forwarding.service
