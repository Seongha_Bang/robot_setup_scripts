#! /bin/bash


if [[ -n "${1}" ]]
then
  password=${1}
  retype=${1}
else
  echo -n "Enter new password: "
  read -s -r password
  echo

  echo -n "Retype new password: "
  read -s -r retype
  echo
fi


if [[ "${password}" == "${retype}" ]]
then
  gsettings set org.gnome.Vino vnc-password $(echo -n "${password}" | base64)
  echo "Success"
else
  echo "Failed"
fi
