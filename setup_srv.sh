#!/bin/bash

DIR="$(dirname "$(realpath -s "${BASH_SOURCE}")")"


# Add user to sudoers
bash ${DIR}/sudo.sh
echo

# Execute main(minimum) setup scripts
bash ${DIR}/setup_min.sh

# Execute setup scripts
echo -e "[ Execute setup scripts ]"

bash ${DIR}/ssh.sh 8078
bash ${DIR}/vino-vnc.sh
bash ${DIR}/ros.sh 192.168.42.78
bash ${DIR}/jtop.sh
bash ${DIR}/v4l2loopback.sh
bash ${DIR}/TouchScreenRotation/auto_rotation.sh

echo -e "\n--- Done --- \n"

