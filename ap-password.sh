#! /bin/bash


if [[ -n "${1}" ]]
then
  password=${1}
  retype=${1}
else
  echo -n "Enter new password: "
  read -s -r password
  echo

  echo -n "Retype new password: "
  read -s -r retype
  echo
fi


if [[ "${password}" == "${retype}" ]]
then
  sudo sed -i "s/^wpa_passphrase=.*/wpa_passphrase=${password}/g" /etc/hostapd/hostapd.conf
  sudo sed -i "s/password: .*/password: \"${password}\"/g" /etc/netplan/50-dogu-ap.yaml
  echo "Success"
else
  echo "Failed"
fi
