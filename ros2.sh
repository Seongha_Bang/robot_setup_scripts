#! /bin/bash

TARGET_ROS_VERSION="humble"


function TaskResult() {
  if [[ $? -eq 0 ]]
  then
    echo "[Setup] Done"
  else
    echo "[Error] Failed" >&2
  fi
}

function UpdatePkgList() {
  echo
  echo "[Setup] Update pakage list"
  bash -c "sudo apt-get update" >/dev/null

  TaskResult
}

function InstallPkg() {
  echo
  echo "[Setup] Install $1"
  bash -c "sudo apt-get install $2 -y" >/dev/null

  TaskResult
}

function SetupTask() {
  echo
  echo "[Setup] $1"
  bash -c "$2" >/dev/null

  TaskResult
}


UpdatePkgList

InstallPkg \
  "assistance pakages" \
  "curl gnupg2 lsb-release locales software-properties-common"

SetupTask \
  "setup locales" \
  "sudo locale-gen en_US en_US.UTF-8;  sudo update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8;  export LANG=en_US.UTF-8"

SetupTask \
  "authorize GPG key" \
  "sudo add-apt-repository universe -y;  sudo curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -o /usr/share/keyrings/ros-archive-keyring.gpg"

SetupTask \
  "Add repository to sources list" \
  "echo \"deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu $(. /etc/os-release && echo $UBUNTU_CODENAME) main\" | sudo tee /etc/apt/sources.list.d/ros2.list >/dev/null"


UpdatePkgList

InstallPkg \
  "ROS $TARGET_ROS_VERSION (desktop)" \
  "ros-$TARGET_ROS_VERSION-desktop"

InstallPkg \
  "ROS $TARGET_ROS_VERSION (base)" \
  "ros-$TARGET_ROS_VERSION-ros-base"

InstallPkg \
  "Dev tools for ROS" \
  "ros-dev-tools"

bash -c "source /opt/ros/$TARGET_ROS_VERSION/setup.bash"


InstallPkg "pip3" "python3-pip"
SetupTask "Install argcomplete" "pip3 install -U argcomplete"

InstallPkg "libpython3-dev" "libpython3-dev"
InstallPkg "python3-argcomplete" "python3-argcomplete"
InstallPkg "colcon compiler" "python3-colcon-common-extensions"
