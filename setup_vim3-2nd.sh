#!/bin/bash

DIR="$(dirname "$(realpath -s "${BASH_SOURCE}")")"


# Execute main(minimum) setup scripts
bash ${DIR}/setup_min.sh

# Execute setup scripts
echo -e "[ Execute setup scripts ]"

bash ${DIR}/ssh.sh 8080
bash ${DIR}/vino-vnc.sh 5902
bash ${DIR}/dummy_screen.sh

echo -e "\n--- Done --- \n"

