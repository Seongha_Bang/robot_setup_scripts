#!/bin/bash


if [[ -z "$(grep 'pcie_aspm=off' /etc/default/grub)" ]]
then
  echo "[GRUB] Add 'pcie_aspm=off' on 'GRUB_CMDLINE_LINUX_DEFAULT'."
  sed "s/^GRUB_CMDLINE_LINUX_DEFAULT=.*/GRUB_CMDLINE_LINUX_DEFAULT=\"quiet splash pcie_aspm=off\"/g" /etc/default/grub | sudo tee /etc/default/grub >/dev/null
  sudo update-grub
else
  echo "[GRUB] 'pcie_aspm=off' option is already on 'GRUB_CMDLINE_LINUX_DEFAULT'."
fi
