#! /bin/bash


echo "[Setup] Install iptables-persistent"

echo iptables-persistent iptables-persistent/autosave_v4 boolean false | sudo debconf-set-selections
echo iptables-persistent iptables-persistent/autosave_v6 boolean false | sudo debconf-set-selections
sudo apt-get install iptables-persistent -y

echo "[Setup] Done"


echo "[Setup] Setting packet forward"

sudo sed -i 's/^#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/g' /etc/sysctl.conf
sudo sed -i 's/^#net.ipv6.conf.all.forwarding=1/net.ipv6.conf.all.forwarding=1/g' /etc/sysctl.conf

echo "[Setup] Done"


echo "[Setup] Select interfaces to share"

if_list=($(ifconfig | grep -Po "^[^\s\n:]+"))
i=0

echo
for name in ${if_list[@]}
do
  i=`expr $i + 1`
  echo "  $i. $name"
done
echo

input=0

while [[ $input -gt ${#if_list[@]} || $input -le 0 ]]
do
  echo -n "Select Number> "
  read input
done

target=${if_list[`expr $input - 1`]}


echo "iptables -t nat -A POSTROUTING -o ${target} -j MASQUERADE"
sudo iptables -t nat -A POSTROUTING -o ${target} -j MASQUERADE

echo "[Setup] Done"


echo "[Setup] Save setting"

sudo netfilter-persistent save

echo "[Setup] Done"


