#!/bin/bash

DIR="$(dirname "$(realpath -s "${BASH_SOURCE}")")"
CURR_DIR=$(pwd)

gitClone() {
  echo -e "[Setup] Cloning ${1} package..."

  git clone $(echo "${2}" | sed 's/gitlab.com/gitlabdoguxyz:dogong2020!!@gitlab.com/g')  # Log in to gitlab with a pull-only account

  echo -e "\n[Setup] ---Done---\n"
}


# Download e-cam files
cd ~/.dogu_system
gitClone "ecam_installation" \
  https://gitlab.com/dogong/agent_ai_system_planner/ecam_installation.git


# Update dtb when first booted from H01 board
bash ${DIR}/setup_h01_jetson_dtb.sh


# Install OAK-D
echo 'SUBSYSTEM=="usb", ATTRS{IDvENDOR}=="03e7", MODE="0666"' | sudo tee /etc/udev/rules.d/80-movidius.rules
sudo udevadm control --reload-rules && sudo udevadm trigger

cd ~/Downloads
sudo curl -fL https://docs.luxonis.com/install_dependencies.sh | bash
git clone https://github.com/luxonis/depthai.git
cd depthai
git checkout v3.0.12
python3 install_requirements.py

cd ~/Downloads
git clone https://github.com/luxonis/depthai-experiments.git
cd depthai-experiments/gen2-face-recognition
python3 -m pip install -r requirements.txt
pip3 install numpy==1.19.4 scikit-learn

cd ~/Downloads/depthai
python3 install_requirements.py

cd ${CURR_DIR}


# Install e-CAM25
echo -e "[ Install e-CAM25 ]"

bash ~/.dogu_system/ecam_installation/ecam.sh

echo -e "\n--- Done --- \n"


# Do fix broken
sudo apt install --fix-broken -y
