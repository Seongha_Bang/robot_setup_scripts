#! /bin/bash


# Install vino vnc
echo -e "[ Setup virtual display ]\n"

sudo apt-get install -y vino

echo -e "\n--- Done --- \n\n"


# Enable remote access
echo -e "[ Enable remote access ]\n"

if [[ -z "$(grep 'Enable remote access' /usr/share/glib-2.0/schemas/org.gnome.Vino.gschema.xml)" ]]
then
  sudo sed -i "$(( $(grep '</key>' /usr/share/glib-2.0/schemas/org.gnome.Vino.gschema.xml -n | cut -d':' -f1 | tail -n 1) + 1 ))i\ \n    <key name='enabled' type='b'>\n     <summary>Enable remote access to the desktop</summary>\n     <description>\n       If true, allows remote access to the desktop via the RFB\n       protocol. Users on remote machines may then connect to the\n       desktop using a VNC viewer.\n     </description>\n     <default>false</default>\n   </key>" /usr/share/glib-2.0/schemas/org.gnome.Vino.gschema.xml
fi

echo -e "\n--- Done --- \n\n"


# Setting vino-server
echo -e "[ Setting vino-server ]\n"

gsettings set org.gnome.Vino prompt-enabled false
gsettings set org.gnome.Vino require-encryption false
sudo glib-compile-schemas /usr/share/glib-2.0/schemas
gsettings set org.gnome.Vino authentication-methods "['vnc']"
# gsettings set org.gnome.Vino vnc-password $(echo -n '0000'|base64)

if [[ -n "$1" ]]
then
  gsettings set org.gnome.Vino alternative-port $1
  gsettings set org.gnome.Vino use-alternative-port true
fi

echo -e "\n--- Done --- \n\n"


# Start vino vnc

export DISPLAY=:0
/usr/lib/vino/vino-server

