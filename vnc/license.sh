#!/bin/bash

DIR=$(dirname $(realpath -s ${BASH_SOURCE}))


# Check root permission

if [ "${EUID}" -eq 0 ]
then
  SUDO=""
else
  SUDO="sudo"
fi


# Update license key

# Offline connection
${SUDO} /usr/bin/vnclicense -add $(cat ${DIR}/key)

## Cloud connection on version 7.x
# ${SUDO} vncserver-x11 -service -setconfig ${DIR}/param
# ${SUDO} vncserver-x11 -service -joinCloud $(cat ${DIR}/key)
