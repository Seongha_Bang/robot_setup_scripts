#!/bin/bash

DIR=$(dirname $(realpath -s ${BASH_SOURCE}))



echo "[Setup] Install VNC Server"

sudo dpkg -i "$(realpath ${DIR}/VNC-Server-*)" >/dev/null

echo "[Setup] Done"


echo


echo "[Setup] Register license key"

bash ${DIR}/license.sh
if [[ $? -eq 0 ]]
then
  echo "[Setup] Done"
else
  echo "[Setup] Fail"
fi


echo


echo "[Setup] Enable and start vncserver-x11-serviced.service"

sudo systemctl enable vncserver-x11-serviced.service
sudo systemctl restart vncserver-x11-serviced.service


echo


if [[ -n "$1" ]]
then
  while [[ -z "$(sudo ls /root/.vnc/config.d/vncserver-x11 2>/dev/null)" ]]
  do
    sleep 1
  done

  echo "[Setup] Change port to $1"

  if [[ $(sudo tail -n 1 /root/.vnc/config.d/vncserver-x11) =~ "RfbPort=" ]]
  then
    sudo sed -i '$d' /root/.vnc/config.d/vncserver-x11
  fi
  echo "RfbPort=$1" | sudo tee -a /root/.vnc/config.d/vncserver-x11 >/dev/null

  sudo systemctl restart vncserver-x11-serviced.service

  echo "[Setup] Done"
fi

echo "[Setup] Done"
