#!/bin/bash


# --- Setup /etc/pulse/default.pa ---

sudo sed -i 's/^load-module module-switch-on-port-available$/# load-module module-switch-on-port-available/g' /etc/pulse/default.pa

# if [[ -z "$(grep -P "^load-module module-device-manager$" /etc/pulse/default.pa)" ]]
# then
#   echo -e "\n### Dogu (for add label to sound device)\nload-module module-device-manager" | sudo tee -a /etc/pulse/default.pa >/dev/null
# fi


# --- Update udev ---

source_list=( $(echo $(pacmd list-sources | grep -Po '(?<=name:\s\<)[^\>]+' | grep -v '.monitor$')) )
disable_list=(  )

for source in ${source_list[@]}
do
card_name=$(pacmd list-sources | sed -n "/${source}/,\$p" | grep -Po '(?<=card:\s)[^>]+' | grep -Po '(?<=<)[^\n]+' | head -n 1)
card_path=$(pacmd list-sources | sed -n "/${source}/,\$p" | grep -Po '(?<=sysfs.path = ")[^"]+' | head -n 1)
card_label=$(python3 -c "import sys
import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GObject


class MicMonitor:
    COLORS = [[[1.0, 1.0, 1.0, 0.5], [1.0, 1.0, 1.0, 0.5]], [[1.0, 0.0, 0.0, 0.5], [0.0, 0.0, 1.0, 0.5]]]

    def __init__(self, source=None):
        self.loop = GObject.MainLoop()
        GObject.threads_init()
        Gst.init(None)

        self.callbacks = {
            'GstLevel': self._callback_GstLevel,
        }

        self.wave_info = [{'rms': 0, 'peak': 0, 'duration': 0, 'listening': False}]

        if source is None:
            source_device = ''
        else:
            source_device = f'device={source}'

        self.pipeline = Gst.parse_launch(
            f'pulsesrc {source_device} ! level name=level interval=60000000 ! fakesink'
        )

        bus = self.pipeline.get_bus()
        bus.add_signal_watch()
        bus.connect('message', self._bus_message_cb)

    def _bus_message_cb(self, bus, message):
        try:
            self.callbacks[type(message.src).__name__](bus, message)
        except KeyError:
            pass
        except Exception as e:
            self.stop()

    def _callback_GstLevel(self, bus, message):
        data = message.get_structure()

        rms = data.get_value('peak')

        if rms:
            rms_avg = 0
            channels = len(rms)

            for channel in range(channels):
                rms_avg += round(10 ** (rms[channel] / 20.0), 2)

            rms_avg /= channels

            try:
                print(int(rms_avg * 100), flush=True)
            except (BrokenPipeError, IOError):
                self.stop()

    def status(self):
        return self.pipeline.next_state == Gst.State.PLAYING

    def listen(self):
        self.pipeline.set_state(Gst.State.PLAYING)
        self.loop.run()

    def stop(self):
        sys.stdout.close()
        sys.stderr.close()
        self.pipeline.set_state(Gst.State.NULL)
        self.loop.quit()

    def __del__(self):
        self.stop()


if __name__ == '__main__':
    if len(sys.argv) > 1:
        target = sys.argv[1]
    else:
        target = None

    mic = MicMonitor(target)
    mic.listen()
" ${source} |\
python3 -c "import tkinter as tk
import tkinter.ttk


class ProgressDialog:
    RESULT = {
        'front': 'front',
        'left':  'left',
        'rear':  'rear',
        'right': 'right',
        'pass':  '',
        'exit':  '__EXIT__',
    }

    def __init__(self, width=640, height=180):
        self.handle = {
            'root': tk.Tk(),
            'progress': tk.IntVar(),
        }

        self.handle['root'].geometry(
            str(width) + 'x' + str(height) + '+' +
            str(int(self.handle['root'].winfo_screenwidth()/2.0-width/2.0)) + '+' +
            str(int(self.handle['root'].winfo_screenheight()/2.0-height/2.0))
        )
        self.handle['root'].title('pulse_labeling')

        self.handle['name'] = tk.Label(self.handle['root'], text='name: ${card_name}')
        self.handle['name'].pack(side=tk.TOP, anchor='w')

        self.handle['path'] = tk.Label(self.handle['root'], text='path: ${card_path}')
        self.handle['path'].pack(side=tk.TOP, anchor='w')

        self.handle['bar'] = tk.ttk.Progressbar(self.handle['root'], maximum=100, variable=self.handle['progress'])
        self.handle['bar'].pack(side=tk.TOP, fill=tk.BOTH, expand=True)

        self.handle['frame_button'] = tk.Frame(self.handle['root'])
        self.handle['frame_button'].pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)

        handle_button = {
            'button_front': tk.Button(self.handle['root'], text='Front', command=lambda: self._callback('front')),
            'button_left':  tk.Button(self.handle['root'], text='Left',  command=lambda: self._callback('left')),
            'button_rear':  tk.Button(self.handle['root'], text='Rear',  command=lambda: self._callback('rear')),
            'button_right': tk.Button(self.handle['root'], text='Right', command=lambda: self._callback('right')),
            'label_margin': tk.Label(self.handle['root']),
            'button_pass':  tk.Button(self.handle['root'], text='Pass',  command=lambda: self._callback('pass')),
            'button_exit':  tk.Button(self.handle['root'], text='Exit',  command=lambda: self._callback('exit')),
        }

        for h in handle_button.values():
            h.pack(in_=self.handle['frame_button'], side=tk.LEFT, fill=tk.BOTH, expand=True)

        self.handle.update(handle_button)

    def __del__(self):
        self.handle['root'].quit()

    def _callback(self, button_type):
        print(self.RESULT[button_type])
        self.stop()

    def main(self):
        try:
            self.handle['root'].update()
        except:
            self.stop()
            return False
        else:
            try:
                value = int(input())
            except:
                pass
            else:
                self.handle['progress'].set(value)
            return True

    def stop(self):
        self.handle['root'].quit()
        try:
            self.handle['root'].destroy()
        except:
            pass


if __name__ == '__main__':
    dialog = ProgressDialog()

    while dialog.main():
        pass

    dialog.stop()
")

if [[ -n "${card_label}" ]]
then
  if [[ "${card_label}" == "__EXIT__" ]]
  then
    exit
  else
    udev_content+="ACTION==\"change\", SUBSYSTEM==\"sound\", DEVPATH==\"$(echo "${card_path}" | grep -Po '.+(?=\d+)')*\", ATTR{id}=\"${card_label}_dev\", ENV{PULSE_NAME}=\"${card_label}_card\", ENV{SOUND_DESCRIPTION}=\"${card_label}\"\n"
  fi
else
  disable_list+=( "${card_name}" )
fi
done

echo -e "${udev_content}" | sudo tee /etc/udev/rules.d/99-dogu-audio-label.rules


# --- Reload audio system ---

sudo udevadm control --reload
sudo udevadm trigger
pulseaudio --kill
pulseaudio --start

until pulseaudio --check
do
  sleep 1
done


# --- Set 4-way card volume ---

for dir in front left rear right
do
  pacmd set-sink-volume $(pacmd list-sinks | grep -P 'name:\s' | grep -Po "[^<]+${dir}[^>]+" | head -n 1) 65535
  pacmd set-source-volume $(pacmd list-sources | grep -P 'name:\s' | grep -Po "[^<]+${dir}[^>]+" | grep -v '.monitor$' | head -n 1) 65535
done


# --- Set default sink/source (front) ---

front_sink="$(pacmd list-sinks | grep -P 'name:\s' | grep -Po '[^<]+front[^>]+' | head -n 1)"
front_source="$(pacmd list-sources | grep -P 'name:\s' | grep -Po '[^<]+front[^>]+' | grep -v '.monitor$' | head -n 1)"

sink_cmd="set-default-sink ${front_sink}"
source_cmd="set-default-source ${front_source}"

# Get line number of existing settings
line_num=$(grep -n '### dogu default device' /etc/pulse/default.pa | grep -Po '^\d+')
line_len=$(tail -n +${line_num} /etc/pulse/default.pa | grep -Pn '^$' | grep -Po '^\d+')

# Clear existing settings
if [[ -n "${line_num}" ]]
then
  if [[ -z "${line_len}" ]]
  then
    sudo sed -i "${line_num},\$d" /etc/pulse/default.pa
  else
    (( line_len -= 1 ))
    sudo sed -i "${line_num},$(( line_num + line_len ))d" /etc/pulse/default.pa
  fi
fi

# Remove last blank lines
sudo sed -i -e :a -e '/^\n*$/{$d;N;ba' -e '}' /etc/pulse/default.pa

# Add label to /etc/pulse/default.pa
echo "

### dogu default device" | sudo tee -a /etc/pulse/default.pa >/dev/null

# Add config to set default device
if [[ -n "${front_sink}" ]] && [[ -n "${front_source}" ]]
then

echo "
${sink_cmd}
${source_cmd}" | sudo tee -a /etc/pulse/default.pa >/dev/null

pacmd ${sink_cmd}
pacmd ${source_cmd}

fi

# Add config to disable unused device

for card in ${disable_list[@]}
do
  echo "set-card-profile ${card} off" | sudo tee -a /etc/pulse/default.pa >/dev/null
  pacmd set-card-profile ${card} off
done

