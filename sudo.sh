#!/bin/bash


if [[ -z "$(sudo grep -P "^${USER}[\t\s]+" /etc/sudoers)" ]]
then
  echo "[sudo] Add ${USER} to sudoers."
  echo -e "\n${USER} ALL=NOPASSWD:ALL" | sudo tee -a /etc/sudoers >/dev/null
else
  echo "[sudo] User ${USER} is already sudoers!"
fi
