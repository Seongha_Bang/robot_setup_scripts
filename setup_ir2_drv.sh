#!/bin/bash

DIR="$(dirname "$(realpath -s "${BASH_SOURCE}")")"


# Add user to sudoers
bash ${DIR}/sudo.sh
echo

# Execute main(minimum) setup scripts
bash ${DIR}/setup_min.sh

# Setup local network
bash ${DIR}/netplan-ips.sh '192.168.42.1' '192.168.42.68/24'

# Prepare dogu_initialer
bash ${DIR}/dogu_initialer/add-autostart.sh ir2


# Execute setup scripts
echo -e "[ Execute setup scripts ]"

bash ${DIR}/ssh.sh 8068
bash ${DIR}/vnc/vnc.sh 10059

bash ${DIR}/ros.sh 192.168.42.68
bash ${DIR}/ros_pkg_setup/IR2_Drv/install.sh

bash ubuntu_router_all.sh

bash ${DIR}/node-exporter.sh
bash ${DIR}/ap.sh

echo -e "\n--- Done --- \n"

