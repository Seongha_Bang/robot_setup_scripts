#!/bin/bash

BROKER_IP=192.168.42.78
BROKER_PORT=1883
BROKER_USER=dogu
BROKER_PASS='dogong123!'


# Check root permission

if [ "${EUID}" -eq 0 ]
then
  SUDO=""
else
  SUDO="sudo"
fi


# Ensure systemd service

if [[ ! -f /etc/systemd/system/dogu-time-sync.service ]]
then
  echo "[Unit]
Description=dogu time subscriber
After=network-online.target

[Service]
ExecStart=/bin/bash ${HOME}/.dogu_system/robot_setup_scripts/time-sync/subscriber.sh
Restart=on-failure
RestartSec=10

[Install]
WantedBy=multi-user.target
" | ${SUDO} tee /etc/systemd/system/dogu-time-sync.service
  ${SUDO} systemctl enable dogu-time-sync.service
fi


# Receive and apply datetime

while true
do
  mosquitto_sub  \
    -h ${BROKER_IP}  \
    -p ${BROKER_PORT}  \
    -u ${BROKER_USER}  \
    -P "${BROKER_PASS}"  \
    -t 'local/datetime' |\
  while read line
  do
    ${SUDO} date -s "$line"
  done
done

