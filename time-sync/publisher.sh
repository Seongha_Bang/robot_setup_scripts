#!/bin/bash

BROKER_IP=192.168.42.78
BROKER_PORT=1883
BROKER_USER=dogu
BROKER_PASS='dogong123!'

MSG_DELAY=60


# Check root permission

if [ "${EUID}" -eq 0 ]
then
  SUDO=""
else
  SUDO="sudo"
fi


# Ensure systemd service

if [[ ! -f /etc/systemd/system/dogu-time-sync.service ]]
then
  echo "[Unit]
Description=dogu time publisher
After=network-online.target

[Service]
ExecStart=/bin/bash ${HOME}/.dogu_system/robot_setup_scripts/time-sync/publisher.sh
Restart=on-failure
RestartSec=10

[Install]
WantedBy=multi-user.target
" | ${SUDO} tee /etc/systemd/system/dogu-time-sync.service
  ${SUDO} systemctl enable dogu-time-sync.service
fi


# Publish datetime

while sleep ${MSG_DELAY}
do
  mosquitto_pub  \
    -h ${BROKER_IP}  \
    -p ${BROKER_PORT}  \
    -u ${BROKER_USER}  \
    -P "${BROKER_PASS}"  \
    -t 'local/datetime'  \
    -m "$(date +'%Y/%m/%d %H:%M:%S %Z')"
done

