#!/bin/bash


# --- Check is host ---
host_ip=${1}

if [[ -z "${host_ip}" ]]
then
  echo "<Usage>"
  echo "${0} {IP of Server}"
  exit
fi


# --- Init param for config ---
pulse_config="/etc/pulse/default.pa"
declare -A pulse_option=(
  ["module-native-protocol-tcp"]="auth-ip-acl=127.0.0.1;192.168.42.0\/24"
)

if [[ -n "$(hostname --all-ip-addresses | grep ${host_ip})" ]]
then
  pulse_option["module-zeroconf-publish"]=""
else
  pulse_option["module-zeroconf-discover"]=""
fi

avahi_config="/etc/avahi/avahi-daemon.conf"
declare -A avahi_option=(
  ["use-ipv4"]="true"
  ["use-ipv6"]="false"
  ["publish-a-on-ipv6"]="no"
  ["publish-aaaa-on-ipv4"]="no"
)


# --- Install zeroconf ---
sudo apt-get install -y pulseaudio-module-zeroconf


# --- Setup pulse ---
for key in ${!pulse_option[@]}
do
  if [[ -n "$(grep "${key}" ${pulse_config})" ]]
  then

    # Edit existing config
    if [[ -n "${pulse_option[${key}]}" ]]
    then
      sudo sed -i "s/^#*load-module ${key}.*/load-module ${key} ${pulse_option[${key}]}/g" ${pulse_config}  # Set option with param
    else
      sudo sed -i "s/^#*load-module ${key}.*/load-module ${key}/g" ${pulse_config}  # Set option without param
    fi

  else

    # Add new config
    if [[ -n "${pulse_option[${key}]}" ]]
    then
      echo "load-module ${key} ${pulse_option[${key}]}" | sudo tee -a ${pulse_config}  # Set option with param
    else
      echo "load-module ${key}" | sudo tee -a ${pulse_config}  # Set option without param
    fi

  fi
done


# --- Setup avahi ---
for key in ${!avahi_option[@]}
do
  sudo sed -i "s/^#*${key}=.*/${key}=${avahi_option[${key}]}/g" ${avahi_config}
done

