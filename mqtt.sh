#! /bin/bash

echo "[Install] Preparing for install"
sudo apt-add-repository ppa:mosquitto-dev/mosquitto-ppa -y >/dev/null
sudo apt-get update >/dev/null

echo "[Install] Install mosquitto server and client"
sudo apt-get install mosquitto -y >/dev/null
sudo apt-get install mosquitto-clients -y >/dev/null

echo "[Setup] Setting MQTT config file"
index=1
set_point=0

cat /etc/mosquitto/mosquitto.conf | while read line
do
  if [[ $set_point -eq 1 ]]
  then
    if [[ ! $line =~ "listener 1883 0.0.0.0" ]]
    then
      echo $index
      sudo sed -i "${index}i allow_anonymous true" /etc/mosquitto/mosquitto.conf
      sudo sed -i "${index}i listener 1883 0.0.0.0" /etc/mosquitto/mosquitto.conf
      sudo sed -i "${index}i \ " /etc/mosquitto/mosquitto.conf
    fi

    break
  fi

  if [[ ! $line =~ '#' ]]
  then
    set_point=1
  fi

  index=$(expr $index + 1)
done

