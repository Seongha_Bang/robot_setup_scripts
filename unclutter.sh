#! /bin/bash


sudo apt install unclutter -y

echo "[Unit]
Description=unclutter
After=network-online.target

[Service]
ExecStart=/usr/bin/unclutter -idle 3
Restart=on-failure
RestartSec=5s
StandardOutput=null

[Install]
WantedBy=default.target" | sudo tee /usr/lib/systemd/user/unclutter.service >/dev/null

systemctl --user enable unclutter.service
systemctl --user restart unclutter.service

