#! /bin/bash


# Check root permission
if [ "${EUID}" -eq 0 ]
then
  SUDO=""
else
  SUDO="sudo"
fi


echo "[Setup] Install iptables-persistent"

echo iptables-persistent iptables-persistent/autosave_v4 boolean false | ${SUDO} debconf-set-selections
echo iptables-persistent iptables-persistent/autosave_v6 boolean false | ${SUDO} debconf-set-selections

${SUDO} apt-get install iptables-persistent -y

echo "[Setup] Done"


echo "[Setup] Setting packet forward"

${SUDO} sed -i 's/^#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/g' /etc/sysctl.conf
${SUDO} sed -i 's/^#net.ipv6.conf.all.forwarding=1/net.ipv6.conf.all.forwarding=1/g' /etc/sysctl.conf

echo "[Setup] Done"


echo "[Setup] Setting network sharing"

if_list=($(ifconfig | grep -Po "^[ew][^\s\n:]*"))
for target in ${if_list[@]}
do
  echo "iptables -t nat -A POSTROUTING -o ${target} -j MASQUERADE"
  ${SUDO} iptables -t nat -A POSTROUTING -o ${target} -j MASQUERADE
done

echo "[Setup] Done"


echo "[Setup] Save setting"

${SUDO} netfilter-persistent save

echo "[Setup] Done"


