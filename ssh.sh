#! /bin/bash

if [[ -n "$1" ]]
then
  PORT=$1
else
  PORT=8068
fi


echo "[Setup] Install SSH"
sudo apt-get install -y ssh >/dev/null

if [[ $? -eq 0 ]]
then
  echo "[Setup] Done"
else
  echo "[Setup] Failed"
fi


echo

echo "[Setup] Setting SSH port"

sudo sed -i '/^Port/d' /etc/ssh/sshd_config
sudo sed -i '/^#\s*Port/d' /etc/ssh/sshd_config

if [[ -d /etc/ssh/sshd_config.d ]]
then
  echo -e "Port 22\nPort ${PORT}" | sudo tee /etc/ssh/sshd_config.d/dogu.conf
else
  sudo sed -i 'N;s/^#AddressFamily/#Port 22\n#AddressFamily/g' /etc/ssh/sshd_config
  sudo sed -i "s/^#Port 22/Port 22\nPort ${PORT}/g" /etc/ssh/sshd_config
fi

echo "[Setup] Done"

