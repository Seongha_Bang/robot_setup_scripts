#!/bin/bash

DIR="$(dirname "$(realpath -s "${BASH_SOURCE}")")"


# Install torch (for python 3.6)
echo -e "[ Install torch (for python 3.6) ]\n"

cd /tmp
wget https://nvidia.box.com/shared/static/p57jwntv436lfrd78inwl7iml6p13fzh.whl -O torch-1.8.0-cp36-cp36m-linux_aarch64.whl
sudo apt-get install -y python3-pip libopenblas-base libopenmpi-dev 
pip3 install Cython
pip3 install numpy torch-1.8.0-cp36-cp36m-linux_aarch64.whl
cd ${DIR}

echo -e "\n--- Done --- \n\n"

# Install torchvision v0.9.0 (for python3.6)
echo -e "[ Install torchvision v0.9.0 (for python3.6) ]\n"

cd /tmp
sudo apt-get -y install libjpeg-dev zlib1g-dev libpython3-dev libavcodec-dev libavformat-dev libswscale-dev
git clone --branch v0.9.0 https://github.com/pytorch/vision torchvision
cd torchvision
export BUILD_VERSION=0.9.0
python3 setup.py install --user
cd ${DIR}

echo -e "\n--- Done --- \n\n"

# Install OpenCV 4.4
echo -e "[ Install OpenCV 4.4 ]\n"

cd /tmp
wget https://raw.githubusercontent.com/mdegans/nano_build_opencv/master/build_opencv.sh
chmod +x build_opencv.sh
./build_opencv.sh
cd ${DIR}

echo -e "\n--- Done --- \n\n"

# Install PyTorch Lightning 1.3.8
echo -e "[ Install PyTorch Lightning 1.3.8 ]\n"

python3 -m pip install --upgrade pip setuptools wheel
pip3 install pytorch-lightning==1.3.8

echo -e "\n--- Done --- \n\n"

