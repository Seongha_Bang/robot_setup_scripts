#! /bin/bash

AP_IP_BAND=60
DIR="$(dirname "$(realpath -s "${BASH_SOURCE}")")"

bash ${DIR}/ap-driver.sh
echo

sleep 1
ap_list=( $(ifconfig | grep -Po '^(\w+):' | grep -Po 'wlx\w+') )
[[ ${#ap_list[@]} -eq 0 ]] && ap_list=( $(ifconfig | grep -Po '^(\w+):' | grep -Po '\w+') )

if [[ ${#ap_list[@]} -gt 0 ]]
then
  if [[ ${#ap_list[@]} -eq 1 ]]
  then
    echo "  AP detect: ${ap_list[0]}"
    target=${ap_list[0]}
  else
    i=0

    for name in ${ap_list[@]}
    do
      i=`expr $i + 1`
      echo "  $i. $name"
    done
    echo

    input=0

    while [[ $input -gt ${#ap_list[@]} || $input -le 0 ]]
    do
      echo -n "Select Number> "
      read input
    done

    target=${ap_list[`expr $input - 1`]}
  fi

  echo
  echo -n "SSID (AP Name)> "
  read ssid

  echo
  echo "[Setup] Installing DHCP server"
  sudo apt-get -y install isc-dhcp-server >/dev/null

  if [[ $? -eq 0 ]]
  then
    echo "[Setup] Done"

  else
    echo "[Error] Install failed" >&2
  fi

  conf_tail=( $(tail /etc/dhcp/dhcpd.conf -n 7) )

  if [[ ${conf_tail[0]} =~ "#" ]]
  then
    string="\nsubnet 192.168.${AP_IP_BAND}.0 netmask 255.255.255.0 {\n  range 192.168.${AP_IP_BAND}.200 192.168.${AP_IP_BAND}.250;\n  option domain-name-servers 8.8.8.8;\n  option subnet-mask 255.255.255.0;\n  option routers 192.168.${AP_IP_BAND}.1;\n  interface ${target};\n}"
    echo -e "$string" | sudo tee -a /etc/dhcp/dhcpd.conf > /dev/null

    sudo sed -i 's/default-lease-time 600;/default-lease-time -1;/g' /etc/dhcp/dhcpd.conf
    sudo sed -i 's/max-lease-time 7200;/max-lease-time -1;/g' /etc/dhcp/dhcpd.conf
  fi

  echo
  echo "[Setup] Setting AP interface"

  if [[ ! -f "/etc/netplan/50-dogu-ap.yaml" ]]
  then
    echo "network:
  version: 2
  renderer: NetworkManager
  wifis:
    ${target}:
      dhcp4: no
      addresses: [192.168.${AP_IP_BAND}.1/24]
      access-points:
        \"${ssid}\":
           password: \"${ssid}@dg0316!\"
           mode: ap
" | sudo tee "/etc/netplan/50-dogu-ap.yaml"
  fi
  echo "[Setup] Done"

  echo
  echo "[Setup] Restart network services"
  sudo systemctl restart NetworkManager.service
  sudo systemctl unmask isc-dhcp-server
  sudo systemctl enable isc-dhcp-server
  sudo systemctl restart isc-dhcp-server
  echo "[Setup] Done"

  echo
  echo "Complete"
else
  echo "[Error] AP not found" >&2
fi
