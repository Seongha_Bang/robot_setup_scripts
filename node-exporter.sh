#!/bin/bash

DIR="$(dirname "$(realpath -s "${BASH_SOURCE}")")"
CURR_DIR=$(pwd)

ARCHITECTURE=$(dpkg --print-architecture)


cd ${HOME}

wget https://github.com/prometheus/node_exporter/releases/download/v1.5.0/node_exporter-1.5.0.linux-${ARCHITECTURE}.tar.gz
tar xvfz node_exporter-*.*-${ARCHITECTURE}.tar.gz

echo "[Unit]
Description=node-exporter
After=network-online.target

[Service]
Type=simple
SyslogIdentifier=node-exporter
Restart=always
RestartSec=5s
ExecStart=$(realpath node_exporter-*)/node_exporter

[Install]
WantedBy=multi-user.target" | sudo tee /etc/systemd/system/node-exporter.service >/dev/null

cd ${CURR_DIR}
