#!/bin/bash

# <Arguments>
# 
# netplan-ips.sh {Gateway} {IP/Mask} [IP/Mask] ...
# 
# <Example>
# 
# netplan-ips.sh 192.168.42.1 192.168.42.68/24 192.168.42.78/24
# 

GW=${1}
shift

adresses="$(python3 -c \
"import sys

target=sys.argv[1:]

print(str(target).replace(\"\'\", \"\"))
" $@
)"


if [ "${EUID}" -eq 0 ]  # Check root permission
then
  SUDO=""
else
  SUDO="sudo"
fi


if [[ -z "$(which netplan)" ]]
then
  sudo apt-get install -y netplan.io
fi

if [[ ! -d "/etc/netplan" ]]
then
  ${SUDO} mkdir /etc/netplan
fi


echo \
"network:
  version: 2
  renderer: NetworkManager
  ethernets:
    $(ifconfig | grep "${GW%.*}" -B 1 | grep -Po "^[^:]+(?=:)" -m 1):
      addresses: ${adresses}
      gateway4: ${GW}
" | ${SUDO} tee /etc/netplan/50-dogu-multi-ip.yaml


# # Apply config of netplan
# ${SUDO} netplan apply

