#! /bin/bash

TARGET_ROS_VERSION="melodic"


function TaskResult() {
  if [[ $? -eq 0 ]]
  then
    echo "[Setup] Done"
  else
    echo "[Error] Failed" >&2
  fi
}

function UpdatePkgList() {
  echo
  echo "[Setup] Update pakage list"
  bash -c "sudo apt-get update" >/dev/null

  TaskResult
}

function InstallPkg() {
  echo
  echo "[Setup] Install $1"
  bash -c "sudo apt-get install $2 -y"  # >/dev/null

  TaskResult
}

function SetupTask() {
  echo
  echo "[Setup] $1"
  bash -c "$2" >/dev/null

  TaskResult
}


SetupTask \
  "Set up keys" \
  "sudo -E apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654"

SetupTask \
  "Accept software from packages.ros.org" \
  "sudo sh -c 'echo \"deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main\" > /etc/apt/sources.list.d/ros-latest.list'"

UpdatePkgList

InstallPkg "ROS $TARGET_ROS_VERSION" "ros-$TARGET_ROS_VERSION-desktop-full"
InstallPkg "ROS USB Camera" "ros-$TARGET_ROS_VERSION-usb-cam"

InstallPkg "python pip" "python-pip"

SetupTask "Install rosdep" "sudo pip install -U rosdep"
bash -c "sudo rosdep init" >/dev/null
SetupTask "Update rosdep" "rosdep update"

InstallPkg \
  "ROS build tools" \
  "python-rosinstall python-rosinstall-generator python-wstool build-essential"
InstallPkg \
  "ROS build tools (ninja)"\
  "ninja-build stow"
  # "python3-wstool python3-rosdep ninja-build stow"

InstallPkg \
  "Python3 ROS Melodic packages" \
  "python3 python3-pip python3-rospkg-modules"

SetupTask "ROS Melodic on Python3" "pip3 install rospkg"


if [[ -n "${1}" ]]
then
  ros_ip="${1}"
else
  ros_ip="127.0.0.1"
fi

regex="192\.168\.42\.[0-9]+"
if [[ "${1}" =~ ${regex} ]]
then
  ros_master="http://192.168.42.68:11311"
else
  ros_master="http://\${ROS_IP}:11311"
fi


if [[ -z "$(cat ~/.bashrc | grep 'source /opt/ros/melodic/setup.bash')" ]]
then
  echo "
# Configure ROS environment
source /opt/ros/melodic/setup.bash
if [ -f \"\${HOME}/catkin_ws/install_isolated/setup.bash\" ]
then
  source ~/catkin_ws/install_isolated/setup.bash
fi

# Configure ROS Network
export ROS_IP=${ros_ip}
export ROS_MASTER_URI=${ros_master}

# Configure ROS alias command
alias cw='cd ~/catkin_ws'
alias cs='cd ~/catkin_ws/src'
alias cn='cd ~/catkin_ws && catkin_make_isolated --install --use-ninja -DCMAKE_BUILD_TYPE=Release'
" >>~/.bashrc
fi
