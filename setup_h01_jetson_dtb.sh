#!/bin/bash


if [[ ! -d "$(realpath ~/.config/autostart)" ]]
then
  mkdir ~/.config/autostart
fi

# Update dtb when first booted from H01 board
echo "[Desktop Entry]

Encoding=UTF-8
Version=1.0
Type=Application
Terminal=true

Exec=/bin/bash -c \"if [[ -z \\\"\$(lsusb)\\\" ]]; then echo '[DTB_Update] Start'; echo 'nvidia' | sudo -S dd if=$(realpath ~/.dogu_system/ecam_installation/kernel_tegra194-p2888-0001-p2822-0000_sigheader.dtb.encrypt) of=/dev/nvme0n1p4; echo '[DTB_Update] Done. Please reboot from H01 board.'; while true; do sleep 3600; done; else echo '[DTB_Update] DTB has already been updated.'; read -p '(Press enter to remove auto script)' line && rm ~/.config/autostart/update-dtb.desktop; fi\"
" | tee ~/.config/autostart/update-dtb.desktop >/dev/null

