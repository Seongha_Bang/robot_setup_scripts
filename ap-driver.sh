#! /bin/bash

echo "[Setup] Update pakage list "
sudo apt-get update >/dev/null

if [[ $? -eq 0 ]]
then
  echo "[Setup] Done"
else
  echo "[Error] Update failed" >&2
fi

echo
echo "[Setup] Installing AP"

sudo apt-get install -y dkms bc git >/dev/null
tmp=$?

git clone https://github.com/cilynx/rtl88x2BU_WiFi_linux_v5.3.1_27678.20180430_COEX20180427-5959.git ~/rtl88x2BU_WiFi_linux_v5.3.1_27678.20180430_COEX20180427-5959 >/dev/null

sudo dkms add ~/rtl88x2BU_WiFi_linux_v5.3.1_27678.20180430_COEX20180427-5959 >/dev/null
tmp=$(expr $? + $tmp)

sudo dkms install -m rtl88x2bu -v 5.3.1 >/dev/null
tmp=$(expr $? + $tmp)

sudo modprobe 88x2bu >/dev/null
tmp=$(expr $? + $tmp)

if [[ $tmp -eq 0 ]]
then
  echo "[Setup] Done"
else
  echo "[Error] Install failed" >&2
fi

