#!/bin/bash

DIR="$(dirname "$(realpath -s "${BASH_SOURCE}")")"


# Add user to sudoers
bash ${DIR}/sudo.sh
echo

# Execute main(minimum) setup scripts
bash ${DIR}/setup_min.sh

# Execute setup scripts
echo -e "[ Execute setup scripts ]"

bash ${DIR}/ssh.sh 8068
bash ${DIR}/vnc/vnc.sh 10059
bash ${DIR}/ros.sh 192.168.42.68
bash ${DIR}/ap.sh

echo -e "\n--- Done --- \n"

