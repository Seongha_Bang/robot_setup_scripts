#!/bin/bash


if [[ -z "$(grep 'robot internal network setting' /etc/hosts)" ]]
then
  echo "[hosts] Add host names in '/etc/hosts'."
  echo -e "\n\n# Dogugonggan's robot internal network setting\n192.168.42.68\tdogu-desktop\n192.168.42.78\tnvidia-desktop\n192.168.42.79\tnvidia-desktop" | sudo tee -a /etc/hosts >/dev/null
else
  echo "[hosts] Host names already exist in '/etc/hosts'."
fi
