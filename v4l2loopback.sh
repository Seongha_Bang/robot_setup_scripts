#!/bin/bash

sudo bash -c '
  apt-get remove v4l2loopback-dkms
  apt-get install -y dkms

  mv /usr/lib/aarch64-linux-gnu/libv4l/plugins/nv/libv4l2_nvargus.so /usr/lib/aarch64-linux-gnu/libv4l/plugins/nv/libv4l2_nvargus.so.bak
  
  cd /usr/src
  git clone https://github.com/umlaeute/v4l2loopback.git
  cd v4l2loopback
  git checkout v0.12.5

  cd ..
  ln -s v4l2loopback v4l2loopback-0.12.5

  dkms build -m v4l2loopback/0.12.5
  dkms install -m v4l2loopback/0.12.5

  modprobe -r v4l2loopback
  modprobe v4l2loopback && dmesg | tail -n 1
  modprobe -r v4l2loopback
  
  apt-get install -y v4l-utils
'
