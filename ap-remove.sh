#! /bin/bash
echo

AP_IP_BAND=60

echo "[Remove] Remove DHCP server setting"

conf_tail=( $(tail /etc/dhcp/dhcpd.conf -n 7) )

if [[ ${conf_tail[1]} =~ "192.168.${AP_IP_BAND}.0" ]]
  then
  for ((i=0;i<8;i++))
  do
    sudo sed -i '$d' /etc/dhcp/dhcpd.conf
  done
fi

echo "[Remove] Remove hostapd setting"

if [[ -e /etc/hostapd/hostapd.conf ]]
then
  bash -c "sudo rm /etc/hostapd/hostapd.conf"
fi

if [[ ${conf_tail[0]} =~ "DAEMON_CONF" ]]
then
  sudo sed -i '$d' /etc/default/hostapd
  sudo sed -i '$d' /etc/default/hostapd

  sudo sed -i 's/default-lease-time -1;/default-lease-time 600;/g' /etc/dhcp/dhcpd.conf
  sudo sed -i 's/max-lease-time -1;/max-lease-time 7200;/g' /etc/dhcp/dhcpd.conf
fi

echo "[Remove] Disable hostapd service"

bash -c "sudo systemctl stop hostapd.service"
bash -c "sudo systemctl disable hostapd.service"

echo "[Remove] Remove AP interface setting"

conf_tail=( $(tail /etc/network/interfaces -n 5) )
if [[ $(expr ${conf_tail[7]} : 192.168.${AP_IP_BAND}.1) -ne 0 ]]
then
  for ((i=0;i<6;i++))
  do
    sudo sed -i '$d' /etc/network/interfaces
  done
fi

sudo rm -rf /etc/netplan/50-dogu-ap.yaml

bash -c "sudo systemctl restart NetworkManager.service"

echo "Complete"
