#!/bin/bash

W_DIR="$(pwd)"

gitClone() {
  echo -e "[Setup] Cloning ${1} package..."

  git clone $(echo "${2}" | sed 's/gitlab.com/gitlabdoguxyz:dogong2020!!@gitlab.com/g')  # Log in to gitlab with a pull-only account

  echo -e "\n[Setup] ---Done---\n"
}


cd ~
gitClone "Dogu AI Package" \
  "https://gitlab.com/dogong/agent_ai_system_planner/d-ai/release-v1-ai-package.git"

cd release-v1-ai-package
bash install.sh

cd ${W_DIR}

