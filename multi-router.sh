#!/bin/bash

# < Note >
# 
# This script separates traffic that uses
# remote access (port forwarding) and the Internet
# when two routers are connected.


# < Parameters >

# Remote Router with Port Forwarding
REMOTE_GW='192.168.42.1'

# Host on local AP that shouldn't be filtered
AP_HOST_IP='192.168.42.68'

# Parameters used in the routing table
# of packets to be filtered
TABLE_NAME='dogu_remote'
MARK_NUM='42'


# < Code >

# Check root permission
if [ "${EUID}" -eq 0 ]
then
  SUDO=""
else
  SUDO="sudo"
fi

# Get local IP
until [[ -n "${ip_addr}" ]]
do
  ip_addr="$(ifconfig | grep -Po '(?<=inet )192.168.42.\d+')"
done

# Get name of local network interface
until [[ -n "${if_name}" ]]
do
  if_name="$(ifconfig | grep -B1 -P "${ip_addr}" | grep -Po '^[^:]+(?=: )')"
done


# Set port forwarding case with IP address
case ${ip_addr} in
  "192.168.42.68")
    declare -A table=(
      ['ssh']='8068'
      ['vnc']='10059'
      ['node_exporter']='9100'
    )
    ;;

  "192.168.42.78")
    declare -A table=(
      ['ssh']='8078'
      ['vnc']='5900'
      ['mqtt']='1883'
    )
    ;;

  "192.168.42.79")
    declare -A table=(
      ['ssh']='8079'
      ['vnc']='5901'
    )
    ;;

  *)
    declare -A table=()
    ;;

  esac


# Add port forwarding rules to routing table
# with iptables command
for port in "${table[@]}"
do
  ${SUDO} iptables -t mangle -I OUTPUT -o ${if_name} ! -d ${AP_HOST_IP} -p tcp --sport ${port} -j MARK --set-mark ${MARK_NUM}
  ${SUDO} iptables -t mangle -I OUTPUT -o ${if_name} ! -d ${AP_HOST_IP} -p udp --sport ${port} -j MARK --set-mark ${MARK_NUM}
done

# Create routing tables for apply to target packets
${SUDO} sed -i "/\t${TABLE_NAME}/d" /etc/iproute2/rt_tables >/dev/null
echo -e "${MARK_NUM}\t${TABLE_NAME}" | ${SUDO} tee -a /etc/iproute2/rt_tables >/dev/null

# Set target packets to routing table
${SUDO} ip rule add fwmark ${MARK_NUM} table ${TABLE_NAME}

# Add routing for target packets
until [[ -n "$(${SUDO} ip route list table ${TABLE_NAME})" ]]
do
  ${SUDO} ip route add default via ${REMOTE_GW} dev ${if_name} table ${TABLE_NAME}
  ${SUDO} ip route add default via ${REMOTE_GW} dev ${if_name} metric 60000
done


# Create and enable service to apply routing at boot
if [[ ! -f /etc/systemd/system/dogu-multi-router.service ]]
then
echo "[Unit]
Description=Setup multi router
After=network-online.target

[Service]
ExecStart=/bin/bash ${HOME}/.dogu_system/robot_setup_scripts/multi-router.sh
Restart=on-failure
RestartSec=10

[Install]
WantedBy=multi-user.target" | ${SUDO} tee /etc/systemd/system/dogu-multi-router.service
fi
${SUDO} systemctl enable dogu-multi-router.service

