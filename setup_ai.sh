#!/bin/bash

DIR="$(dirname "$(realpath -s "${BASH_SOURCE}")")"


# # Add user to sudoers
# bash ${DIR}/sudo.sh
# echo

# Execute main(minimum) setup scripts
bash ${DIR}/setup_min.sh

# Execute setup scripts
echo -e "[ Execute setup scripts ]"

bash ${DIR}/ssh.sh 8079
bash ${DIR}/vino-vnc.sh 5901
bash ${DIR}/jtop.sh
bash ${DIR}/v4l2loopback.sh

echo -e "\n--- Done --- \n"

# Build AI packages
bash ${DIR}/ai_build.sh

