#!/bin/bash

DIR="$(dirname "$(realpath -s "${BASH_SOURCE}")")"

gitClone() {
  echo -e "[Setup] Cloning ${1} package..."

  git clone $(echo "${2}" | sed 's/gitlab.com/gitlabdoguxyz:dogong2020!!@gitlab.com/g')  # Log in to gitlab with a pull-only account

  echo -e "\n[Setup] ---Done---\n"
}


# # Add user to sudoers
# bash ${DIR}/sudo.sh
# echo

# Execute main(minimum) setup scripts
bash ${DIR}/setup_min.sh

# Setup local network
bash ${DIR}/netplan-ips.sh '192.168.42.1' '192.168.42.79/24'

# Prepare dogu_initialer
bash ${DIR}/dogu_initialer/add-autostart.sh ir2


# Execute setup scripts
echo -e "[ Execute setup scripts ]"

bash ${DIR}/ssh.sh 8079
bash ${DIR}/vino-vnc.sh 5901
bash ${DIR}/jtop.sh
bash ${DIR}/v4l2loopback.sh

echo -e "\n--- Done --- \n"

# # Build AI packages
# bash ${DIR}/ai_build.sh


# Install ROS for docking camera
echo -e "[ Install ROS ]"

bash ${DIR}/ros.sh 192.168.42.79
sudo apt-get install -y ros-melodic-usb-cam

echo -e "\n--- Done --- \n"


# Install camera_virtual_device
echo -e "[ Install camera_virtual_device ]"

W_DIR="$(pwd)"
cd ~/.dogu_system

gitClone "camera_virtual_device" \
  "https://gitlab.com/dogong/agent_ai_system_planner/camera_virtual_device.git"
cd camera_virtual_device
git checkout dev/main

cd ${W_DIR}

echo -e "\n--- Done --- \n"


# Init H01
bash ${DIR}/setup_h01_jetson.sh
