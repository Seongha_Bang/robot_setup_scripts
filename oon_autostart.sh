#!/bin/bash

set -m


# Don't write history
export HISTSIZE=0
export HISTFILE=""

# DNS of oon
export OON_DNS=oon.dogong.xyz

# Default robot_id
export default_robot_id=${1}


# Ensure ~/.config/autostart/oon.desktop

if [[ -z "$(grep '.dogu_system/robot_setup_scripts/oon_autostart.sh' ~/.config/autostart/oon.desktop 2>/dev/null)" ]]
then

mkdir -p ~/.config/autostart

echo "[Desktop Entry]
Type=Application
Exec=/bin/bash -i ${HOME}/.dogu_system/robot_setup_scripts/oon_autostart.sh ${default_robot_id}
Terminal=true
Hidden=false
NoDisplay=false
X-GNOME-Autostart-enabled=true
Name[en_US]=oon
Name=oon
Comment[en_US]=
Comment=
" >~/.config/autostart/oon.desktop

fi


# Main loop
while true
do
  
  # Ensure wmctrl for minimize window 
  
  if [[ -z "$(which wmctrl)" ]]
  then
    sudo apt-get install --no-remove -y wmctrl
  fi
  
  
  # Get robot_id
  
  echo "Wait robot id..."
  
  if [[ -n "${default_robot_id}" ]]
  then
    
    # Use default_robot_id if given
    
    robot_id=${default_robot_id}
    
  else
  
    # Wait for get ID of robot
    
    while [[ -z "$(rosparam get /cloud_robot_id 2>/dev/null)" ]]
    do
      sleep 1
    done
    
    # Get robot_id from ROS
    
    robot_id=$(rosparam get /cloud_robot_id)
    
  fi
  
  echo "ID is ${robot_id}"
  
  
  # Wait for video on virtual device for oon (/dev/video2n)
  
  echo "Wait virtual device..."
  
  while [[ -z "$(ls /dev | grep -P 'video2\d$')" ]]
  do
    sleep 1
  done
  
  for dev in $(ls /dev | grep -P 'video2\d$')
  do
    while [[ -z "$(v4l2-ctl --list-formats-ext -d /dev/${dev} | grep Size)" ]]
    do
      sleep 1
    done
  done
  
  echo "All device is ready!"
  
  
  # Generate oon urls from name of virtual device
  
  url_list=$(
    ls /dev | grep -P 'video2\d$' | while read line
    do
      udev_info=$(udevadm info --attribute-walk /dev/${line})
      cam_name="$(echo "${udev_info}" | grep -Po "(?<=ATTR{name}==\")[^\"]+(?=\")")"
      echo "https://${OON_DNS}/vs/${robot_id}/${cam_name}/${cam_name} "
    done
  )
  
  
  # Start oon
  
  echo "--- START CHROME ---"
  sleep 1
  
  chromium-browser \
    --user-data-dir="${HOME}/.config/$(ls ~/.config | grep -i chrom | grep -v _oon)_oon" \
    --disable-fre \
    --no-default-browser-check \
    --no-first-run \
      ${url_list} \
  &
  pid=${!}
  
  sleep 1
  
  
  # Minimize oon window
  
  chrome_id=""
  
  while [[ -z "${chrome_id}" ]]
  do
    chrome_id=$(wmctrl -l | grep -i chrom | grep -i "security operation system" | tail -1 | while read a b ; do echo ${a} ; done)
    sleep 1
  done
  
  wmctrl -ir ${chrome_id} -b remove,fullscreen
  wmctrl -ir ${chrome_id} -b remove,maximized_vert
  wmctrl -ir ${chrome_id} -b remove,maximized_horz
  wmctrl -ir ${chrome_id} -b add,hidden
  wmctrl -ir ${chrome_id} -b add,shaded
  
  
  # Check internet
  
  while [[ "$(ping -c 5 -W 1 ${OON_DNS} | grep -Po '\d\s*received' | grep -Po '\d')" -ne 0 ]]
  do
    sleep 10
  done
  
  kill ${pid}
  sleep 2
  echo "--- INTERNET HAS DISCONNECTED ---"
  
  while [[ "$(ping -c 1 -W 1 ${OON_DNS} | grep -Po '\d\s*received' | grep -Po '\d')" -eq 0 ]]
  do
    sleep 2
  done
  
done

