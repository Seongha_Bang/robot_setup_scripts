#!/bin/bash

DIR="$(dirname "$(realpath -s "${BASH_SOURCE}")")"


# Remove packages for storage space
ARCHITECTURE=$(dpkg --print-architecture)
if [[ "${ARCHITECTURE}" != "amd64" ]]
then
  sudo apt-get -y remove \
    libreoffice-* \
    thunderbird* \
    gnome-mines \
    gnome-mahjongg \
    gnome-sudoku \
    aisleriot
  sudo apt-get -y autoremove
fi

# Update & Upgrade apt
echo -e "[ Update & Upgrade apt ]"

sudo apt-get update
sudo apt-get upgrade -y

echo -e "\n--- Done --- \n"

# Install packages
echo -e "[ Install packages ]"

sudo apt-get install libpam-kwallet4 libpam-kwallet5  # for graphic manager
sudo apt-get install net-tools netplan.io nmap ifmetric iperf3 nload curl sshfs -y  # network
sudo apt-get install gnome-shell-extensions gnome-shell-extension-autohidetopbar gnome-tweak-tool -y  # for hide top-bar
sudo apt-get install python-pip python3-pip -y  # python
pip2 install --upgrade pip setuptools wheel  # python2 pip
pip3 install --upgrade pip setuptools wheel  # python3 pip
sudo apt-get install glances pavucontrol -y  # monitoring

# for develop
sudo apt-get install parallel -y
pip2 install scikit-learn
pip3 install scikit-learn

# Install Node.js (version: 16.x)
curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash -
sudo apt-get install -y nodejs

echo -e "\n--- Done --- \n"

# Hide side-bar, trash icon
gsettings set org.gnome.shell.extensions.dash-to-dock intellihide false
gsettings set org.gnome.shell.extensions.dash-to-dock pressure-threshold 0
gsettings set org.gnome.nautilus.desktop trash-icon-visible false

# Setup global git account
echo -e "[ Setup global git account ]"

git config --global user.name "gitlabdoguxyz"
git config --global user.email gitlab@dogu.xyz

echo -e "\n--- Done --- \n"

# Install on jetson
if ls /etc/nv_tegra_release >/dev/null 2>&1
then
  bash ${DIR}/jtop.sh          # jtop
  bash ${DIR}/v4l2loopback.sh  # v4l2loopback
fi


# Execute setup scripts
echo -e "[ Execute default setup scripts ]"

if [[ "$(uname -p)" == "x86_64" ]]
then
    bash ${DIR}/grub.sh
fi

bash ${DIR}/disable-screen-saver.sh
bash ${DIR}/ibus.sh
bash ${DIR}/unclutter.sh
bash ${DIR}/chrome.sh
bash ${DIR}/gstreamer.sh
bash ${DIR}/update-notifier.sh
bash ${DIR}/mqtt.sh
bash ${DIR}/ap-driver.sh

echo -e "\n--- Done --- \n"

