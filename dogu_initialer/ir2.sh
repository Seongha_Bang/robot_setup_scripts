#!/bin/bash

DIR="$(dirname "$(realpath -s "${BASH_SOURCE}")")"
ARCHITECTURE=$(dpkg --print-architecture)


declare -A PC_TYPE_TABLE=(
  ["192.168.42.68"]="drv"
  ["192.168.42.78"]="srv"
  ["192.168.42.79"]="ai"
)


set -m  # For job control

bash ${DIR}/self-update.sh  # Update robot_setup_scripts


# Identify type of PC with IP

while [[ -z "${pc_ip}" ]]
do
  pc_ip="$(ifconfig | grep -Po '(?<=inet\s)192\.168\.42\.\d+')"
  sleep 5
done

pc_type=${PC_TYPE_TABLE[${pc_ip}]}


# Run setup script with type of PC

if [[ -f "${DIR}/first_init.txt" ]]
then
  bash ${DIR}/scripts/ir2_${pc_type}/main.sh
else
  bash ${DIR}/onboard-keyboard.sh
  bash ${DIR}/scripts_first/ir2_${pc_type}/main.sh
fi

