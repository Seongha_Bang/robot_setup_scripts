#!/bin/bash

if sudo -n true
then
  sudo apt-get install -y ibus ibus-hangul fonts-nanum* onboard
  ibus restart
fi


cd /usr/share/ibus-hangul/setup/

python3 -c 'import sys
import os
import gi
from gi.repository import GLib
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
gi.require_version("IBus", "1.0")
from gi.repository import IBus
import locale
import gettext
import config
from keycapturedialog import KeyCaptureDialog

from main import Setup


class NewSetup(Setup):
   def add_alt_r(self):
     key_str = "Alt_R"
     model = self._Setup__hangul_key_list.get_model()
     iter = model.get_iter_first()
     while iter:
       str = model.get_value(iter, 0)
       if str == key_str:
         model.remove(iter)
         break
       iter = model.iter_next(iter)
     model.append([key_str])


locale.bindtextdomain(config.gettext_package, config.localedir)
locale.bind_textdomain_codeset(config.gettext_package, "UTF-8")

bus = IBus.Bus()
if bus.is_connected():
  su = NewSetup(bus)
  su.add_alt_r()
  su.apply()
  su._Setup__window.destroy()
'
