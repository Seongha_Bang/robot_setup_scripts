#!/bin/bash

DIR="$(dirname "$(realpath -s "${BASH_SOURCE}")")"
MAIN_DIR="$(realpath -s "${DIR}/../..")"

# apply latest license key
bash ~/.dogu_system/robot_setup_scripts/vnc/license.sh
