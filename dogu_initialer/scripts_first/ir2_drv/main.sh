#!/bin/bash

DIR="$(dirname "$(realpath -s "${BASH_SOURCE}")")"
MAIN_DIR="$(realpath -s "${DIR}/../..")"


# Get parameter or Wait for arguments from Srv PC

robot_name="$(grep -Po '(?<=^\"robot_name\" \")[^\"]+' ${MAIN_DIR}/param.txt)"
robot_id="$(grep -Po '(?<=^\"robot_id\" \")[^\"]+' ${MAIN_DIR}/param.txt)"

while [[ -z "${robot_name}" || -z "${robot_id}" ]]
do
  args=( $(mosquitto_sub -h 192.168.42.78 -C 1 -t 'dogu_initialer/drv/req') )

  robot_name="${args[0]}"
  robot_id="${args[1]}"
done


# Sends response to Srv

for i in {1..5}
do
  mosquitto_pub -h 192.168.42.78 -t 'dogu_initialer/drv/res' -m "${i}"
  sleep 1
done


# Setup password

bash ~/.dogu_system/robot_setup_scripts/password.sh "${robot_name}@dg0316!"


# Setup routing

if [[ -z "$(sudo iptables -t nat -L -n -v | grep MASQUERADE)" ]]
then
  bash ~/.dogu_system/robot_setup_scripts/ubuntu_router_all.sh
fi


# Setup AP

bash ~/.dogu_system/robot_setup_scripts/ap-auto.sh ${robot_name}


# Setup robot.yaml

for id_type in robot_id cloud_robot_id default_robot_id
do
  sed -i "s/^${id_type}: .*/${id_type}: ${robot_id}/g" ~/.dogu_system/robot.yaml
done


# Record the completion
date '+%Y/%m/%d %H:%M:%S' >${MAIN_DIR}/first_init.txt

