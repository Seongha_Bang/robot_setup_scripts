#!/bin/bash

DIR="$(dirname "$(realpath -s "${BASH_SOURCE}")")"
MAIN_DIR="$(realpath -s "${DIR}/../..")"

PC_LIST=( "drv" "ai" )

set -m


# Execute virtual keyboard

nohup onboard -l "$(find ${DIR} -name hangul.onboard)" 1>/dev/null 2>&1 &
disown ${!}


# Enter name of robot (by human)

robot_name="$(grep -Po '(?<=^\"robot_name\" \")[^\"]+' ${MAIN_DIR}/param.txt)"

while [[ -z "${robot_name}" ]]
do
  robot_name=$(
    zenity --entry \
      --title="dogu_initialer" \
      --text="Enter name of robot (Example: IR1001)" \
      2>/dev/null
  )
done

sleep 1.5


# Enter ID of robot (by human)

robot_id="$(grep -Po '(?<=^\"robot_id\" \")[^\"]+' ${MAIN_DIR}/param.txt)"

while [[ -z "${robot_id}" ]]
do
  robot_id=$(
    zenity --entry \
      --title="dogu_initialer" \
      --text="Enter ID of robot (Example: ir-1-1)" \
      2>/dev/null
  )
done

sleep 1.5


# Send signal and arguments to another PC

for target in ${PC_LIST[@]}
do
  while true
  do
    mosquitto_pub \
      -t "dogu_initialer/${target}/req" \
      -m "${robot_name} ${robot_id}"

    if [[ -n "$(timeout 5 mosquitto_sub -C 1 -t "dogu_initialer/${target}/res")" ]]
    then
      break
    fi
  done
done &


# Setup password

bash ~/.dogu_system/robot_setup_scripts/password.sh "${robot_name}@dg0316!"


# Setup SSHFS

echo "#!/bin/bash

if [[ ! -d /mnt/.dogu_system ]]
then
  sudo mkdir /mnt/.dogu_system
fi

while true
do
    result=\"\$(df -hT 2>/dev/null | grep '192.168.42.68')\"
    echo \"[RESULT] \${result}\"

    if [[ -z \"\${result}\" ]]
    then
      killall sshfs
      sudo umount -l /mnt/.dogu_system
      echo '${robot_name}@dg0316!' | sshfs dogu@192.168.42.68:/home/dogu/.dogu_system /mnt/.dogu_system -o password_stdin,allow_other,StrictHostKeyChecking=no
    else
      echo 'SSHFS is already mounted.'
    fi

    sleep 10
done
" | tee ~/.dogu_system/bringup/dogu-sftp.sh

echo "[Unit]
Description=Start Dogu SFTP
After=network-online.target

[Service]
Type=simple
ExecStart=/bin/bash ${HOME}/.dogu_system/bringup/dogu-sftp.sh

[Install]
WantedBy=multi-user.target
" | sudo tee /etc/systemd/system/dogu-sftp.service


# Set robot id on ai_event_publisher
sed -i "s/    robot_id: \d+/    robot_id: ${robot_id}/" ~/.dogu_system/ai/target-*


# Enable services
sudo systemctl enable dogu-sftp
sudo systemctl restart dogu-sftp
systemctl --user enable dogu
systemctl --user restart dogu


# Set default volume on pulseaudio
pacmd set-sink-volume alsa_output.front_card.analog-stereo 65535
pacmd set-sink-volume alsa_output.left_card.analog-stereo 65535
pacmd set-sink-volume alsa_output.rear_card.analog-stereo 65535
pacmd set-sink-volume alsa_output.right_card.analog-stereo 65535
pacmd set-source-volume alsa_input.front_card.analog-mono 65535
pacmd set-source-volume alsa_input.left_card.analog-mono 65535
pacmd set-source-volume alsa_input.rear_card.analog-mono 65535
pacmd set-source-volume alsa_input.right_card.analog-mono 65535


# Record the completion
date '+%Y/%m/%d %H:%M:%S' >${MAIN_DIR}/first_init.txt


wait

