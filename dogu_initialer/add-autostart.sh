#!/bin/bash

DIR="$(dirname "$(realpath -s "${BASH_SOURCE}")")"


if [[ -z "${1}" ]]
then
  echo "usage: ${0} [type of robot]"
  exit
fi


# Create desktop file on autostart
mkdir ~/.config/autostart 2>/dev/null
echo "[Desktop Entry]

Encoding=UTF-8
Version=1.0
Type=Application
Terminal=true

Exec=bash -c \"bash ${DIR}/${1}.sh; echo -e '\\n--- dogu_initialer ended ---'; while true; do sleep 3600; done\"
Path=${DIR}
" > ~/.config/autostart/dogu_initialer.desktop
chmod +x ~/.config/autostart/dogu_initialer.desktop

