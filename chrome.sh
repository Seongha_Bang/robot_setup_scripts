#!/bin/bash


ARCHITECTURE=$(dpkg --print-architecture)
# amd64, arm64, ...

index=1
set_point=0


# Install chrome if the architecture is amd64, or chromium if not.
if [[ "${ARCHITECTURE}" == "amd64" ]]
then
  # Add chrome package's key & PPA
  echo -e "[ Add chrome package's key & PPA before apt update ]"

  wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
  sudo sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google-chrome.list'
  sudo apt-get update

  echo -e "\n--- Done --- \n"

  # Install chrome
  echo -e "[ Install chrome ]"

  sudo apt-get install google-chrome-stable -y
  sudo apt-get --only-upgrade install google-chrome-stable -y
  
  CHROME_DIR=$(which google-chrome)
  cat ${CHROME_DIR} | while read line
  do
    if [[ $set_point -eq 1 ]]
    then
      if [[ ! $line =~ "# Disable restore pages popup" ]]
      then
        echo $index
        sudo sed -i --follow-symlinks "${index}i \ " ${CHROME_DIR}
        sudo sed -i --follow-symlinks "${index}i sed -i 's/\"exit_type\":\"Crashed\"/\"exit_type\":\"Normal\"/' ~/.config/google-chrome/Default/Preferences" ${CHROME_DIR}
        sudo sed -i --follow-symlinks "${index}i # Disable restore pages popup" ${CHROME_DIR}
      fi

      break
    fi

    if [[ ! $line =~ '#' ]]
    then
      set_point=1
    fi

    index=$(expr $index + 1)
  done

  echo -e "\n--- Done --- \n"
else
  # Install chromium
  echo -e "[ Install chromium ]"

  sudo apt-get install chromium-browser -y
  
  CHROME_DIR=$(which chromium-browser)
  cat ${CHROME_DIR} | while read line
  do
    if [[ $set_point -eq 1 ]]
    then
      if [[ ! $line =~ "# Disable restore pages popup" ]]
      then
        echo $index
        sudo sed -i --follow-symlinks "${index}i \ " ${CHROME_DIR}
        sudo sed -i --follow-symlinks "${index}i sed -i 's/\"exit_type\":\"Crashed\"/\"exit_type\":\"Normal\"/' ~/.config/chromium/Default/Preferences" ${CHROME_DIR}
        sudo sed -i --follow-symlinks "${index}i # Disable restore pages popup" ${CHROME_DIR}
      fi

      break
    fi

    if [[ ! $line =~ '#' ]]
    then
      set_point=1
    fi

    index=$(expr $index + 1)
  done

  echo -e "\n--- Done --- \n"
fi


# Disable chrome update
echo -e "[ Disable chrome update ]"

sudo mkdir -p /etc/opt/chrome/policies/managed
echo '{
"ComponentUpdatesEnabled": "false"
}' | sudo tee /etc/opt/chrome/policies/managed/component_update.json

echo -e "\n--- Done --- \n"
