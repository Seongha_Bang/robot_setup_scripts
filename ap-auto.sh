#! /bin/bash

AP_IP_BAND=60
DIR="$(dirname "$(realpath -s "${BASH_SOURCE}")")"

ROBOT_ID="${1}"


if [[ ! -n "$(ls ~/rtl88x2BU_* 2>/dev/null)" ]]
then
  bash ${DIR}/ap-driver.sh
  echo
fi

echo
echo "[Setup] Installing dependency package"
sudo apt-get install -y isc-dhcp-server hostapd >/dev/null


echo "[Setup] Wait for interface to be used as AP"

until [[ -n "${ap_target}" ]]
do
  ap_target=$(ifconfig | grep -Po '^(\w+):' | grep -Po 'wlx\w+' | head -n 1)
  sleep 1
done

echo "[Setup] Target interface: ${ap_target}"


conf_tail=( $(tail /etc/dhcp/dhcpd.conf -n 7) )

if [[ ${conf_tail[0]} =~ "#" ]]
then
  string="\nsubnet 192.168.${AP_IP_BAND}.0 netmask 255.255.255.0 {\n  range 192.168.${AP_IP_BAND}.200 192.168.${AP_IP_BAND}.250;\n  option domain-name-servers 8.8.8.8;\n  option subnet-mask 255.255.255.0;\n  option routers 192.168.${AP_IP_BAND}.1;\n  interface ${ap_target};\n}"
  echo -e "$string" | sudo tee -a /etc/dhcp/dhcpd.conf > /dev/null

  sudo sed -i 's/default-lease-time 600;/default-lease-time -1;/g' /etc/dhcp/dhcpd.conf
  sudo sed -i 's/max-lease-time 7200;/max-lease-time -1;/g' /etc/dhcp/dhcpd.conf
fi


echo
echo "[Setup] Setting AP interface"

if [[ ! -f "/etc/netplan/50-dogu-ap.yaml" ]]
then

echo "network:
  version: 2
  renderer: NetworkManager
  wifis:
    ${ap_target}:
      dhcp4: no
      addresses: [192.168.${AP_IP_BAND}.1/24]
      access-points:
        \"${ROBOT_ID}\":
           password: \"${ROBOT_ID}@dg0316!\"
           mode: ap
" | sudo tee "/etc/netplan/50-dogu-ap.yaml"

fi

echo "[Setup] Done"


echo
echo "[Setup] Restart network services"
sudo systemctl restart NetworkManager.service
sudo systemctl unmask isc-dhcp-server
sudo systemctl enable isc-dhcp-server
sudo systemctl restart isc-dhcp-server
sudo netplan apply
echo "[Setup] Done"

echo
echo "Complete"
