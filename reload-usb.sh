#!/bin/bash

if [[ -n ${1} ]]
then
  driver_name=${1}
else
  driver_name=$(lsusb -t | grep "Class=root_hub" | grep -Po "(?<=Driver=)[^/]+" | uniq -d)
fi

driver_path=$(find /sys/bus/*/drivers/${driver_name} -type l)

bus=${driver_path##*/}
driver=${driver_path%/*}

echo ${bus} | sudo tee ${driver}/unbind >/dev/null
echo ${bus} | sudo tee ${driver}/bind >/dev/null
