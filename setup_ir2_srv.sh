#!/bin/bash

DIR="$(dirname "$(realpath -s "${BASH_SOURCE}")")"

gitClone() {
  echo -e "[Setup] Cloning ${1} package..."

  git clone $(echo "${2}" | sed 's/gitlab.com/gitlabdoguxyz:dogong2020!!@gitlab.com/g')  # Log in to gitlab with a pull-only account

  echo -e "\n[Setup] ---Done---\n"
}


# Add user to sudoers
bash ${DIR}/sudo.sh
echo

# Execute main(minimum) setup scripts
bash ${DIR}/setup_min.sh

# Setup local network
bash ${DIR}/netplan-ips.sh '192.168.42.1' '192.168.42.78/24'

# Prepare dogu_initialer
bash ${DIR}/dogu_initialer/add-autostart.sh ir2


# Execute setup scripts
echo -e "[ Execute setup scripts ]"

bash ${DIR}/ssh.sh 8078
bash ${DIR}/vino-vnc.sh

bash ${DIR}/jtop.sh
bash ${DIR}/v4l2loopback.sh
bash ${DIR}/TouchScreenRotation/auto_rotation.sh

bash ${DIR}/ros.sh 192.168.42.78
bash ${DIR}/ros_pkg_setup/IR2_Srv/install.sh

echo -e "\n--- Done --- \n"


# Install camera_virtual_device
echo -e "[ Install camera_virtual_device ]"

W_DIR="$(pwd)"
cd ~/.dogu_system

gitClone "camera_virtual_device" \
  "https://gitlab.com/dogong/agent_ai_system_planner/camera_virtual_device.git"
cd camera_virtual_device
git checkout dev/main

cd ~/.dogu_system

gitClone "sound_detection" \
  "https://gitlab.com/dogong/agent_ai_system_planner/d-ai/sound_detection.git"
cd sound_detection
git checkout dev/main
bash setup.sh
bash service.sh

cd ~/.dogu_system

gitClone "speech_detection" \
  "https://gitlab.com/dogong/agent_ai_system_planner/d-ai/speech_detection.git"
cd speech_detection
git checkout dev/main
bash service.sh

cd ~/.dogu_system

gitClone "ToolBox" \
  "https://gitlab.com/dogong/service-layer/toolbox.git"
cd toolbox
bash build.sh
bash desktop.sh

cd ${W_DIR}

echo -e "\n--- Done --- \n"


# Create AI visualization
bash ${DIR}/ai_visualization.sh
