#!/bin/bash

TARGET_DIR="/home/${USER}/.dogu_system/ai_visualization"

createFile() {
  if test -f "${1}"
  then
    sudo rm -rf ${1}
  fi
  
  echo "${2}" >> ${1}
  
  sudo chmod +x ${1}
}


# Create AI visualization
echo -e "[ Create AI visualization ]"

sudo apt-get install -y parallel

# Create script folder
W_DIR="$(pwd)"
mkdir -p ${TARGET_DIR}
cd ${TARGET_DIR}


# Create desktop shortcut
createFile \
"/home/${USER}/Desktop/AI.desktop" \
"[Desktop Entry]
Version=1.0
Exec=bash ${TARGET_DIR}/show_all.sh
Name=AI Visualization
GenericName=AI Visualization
Comment=AI Visualization
Terminal=true
Type=Application"

# Create main script
createFile \
"${TARGET_DIR}/show_all.sh" \
'#! /bin/bash
echo "AI Display"

sleep 10

parallel -j8 bash ::: "'"${TARGET_DIR}"'/show_fall.sh" "'"${TARGET_DIR}"'/show_human.sh" "'"${TARGET_DIR}"'/show_fire.sh" "'"${TARGET_DIR}"'/show_helmet.sh"'


# Create fall visualization
createFile \
"${TARGET_DIR}/show_fall.sh" \
'#!/bin/bash
gst-launch-1.0 -v udpsrc port=5013 caps = "application/x-rtp" ! rtph265depay ! h265parse ! nvv4l2decoder enable-max-performance=1 ! nvvidconv ! clockoverlay halignment=left valignment=top text="Cam 5013 " shaded-background=true font-desc="Sans, 12" ! timeoverlay halignment=right valignment=bottom text="Fallen Detection" shaded-background=true font-desc="Sans, 12" ! queue ! xvimagesink sync=false'

# Create fire visualization
createFile \
"${TARGET_DIR}/show_fire.sh" \
'#!/bin/bash
gst-launch-1.0 -v udpsrc port=5011 caps = "application/x-rtp" ! rtph265depay ! h265parse ! nvv4l2decoder enable-max-performance=1 ! nvvidconv ! clockoverlay halignment=left valignment=top text="Cam 5011 " shaded-background=true font-desc="Sans, 12" ! timeoverlay halignment=right valignment=bottom text="Fire Detection" shaded-background=true font-desc="Sans, 12" ! queue ! xvimagesink sync=false'

# Create helmet visualization
createFile \
"${TARGET_DIR}/show_helmet.sh" \
'#!/bin/bash
gst-launch-1.0 -v udpsrc port=5018 caps = "application/x-rtp" ! rtph265depay ! h265parse ! nvv4l2decoder enable-max-performance=1 ! nvvidconv ! clockoverlay halignment=left valignment=top text="Cam 5018 " shaded-background=true font-desc="Sans, 12" ! timeoverlay halignment=right valignment=bottom text="Helmet Detection" shaded-background=true font-desc="Sans, 12" ! queue ! xvimagesink sync=false'

# Create human visualization
createFile \
"${TARGET_DIR}/show_human.sh" \
'#!/bin/bash
gst-launch-1.0 -v udpsrc port=5014 caps = "application/x-rtp" ! rtph265depay ! h265parse ! nvv4l2decoder enable-max-performance=1 ! nvvidconv ! clockoverlay halignment=left valignment=top text="Cam 5014 " shaded-background=true font-desc="Sans, 12" ! timeoverlay halignment=right valignment=bottom text="Human Detection" shaded-background=true font-desc="Sans, 12" ! queue ! xvimagesink sync=false'


# Done
cd ${W_DIR}
echo -e "\n--- Done --- \n"
