#!/bin/bash

DIR="$(dirname "$(realpath -s "${BASH_SOURCE}")")"

if [[ -n "${1}" ]]
then
  SRV_NAME="${1}"
else
  SRV_NAME="dogu"
fi


mkdir ~/.dogu_system/bringup

# Create bringup service file
echo "[Unit]
Description=${SRV_NAME} start
After=network-online.target

[Service]
ExecStart=/bin/bash -i ${HOME}/.dogu_system/bringup/${SRV_NAME}.sh
Restart=on-failure
RestartSec=10

[Install]
WantedBy=default.target" |
sudo tee /etc/systemd/user/${SRV_NAME}.service >/dev/null

# Create bringup script file
echo '#!/bin/bash' "

export HISTSIZE=0


if [[ \$- != *i* ]]
then
  echo \"[dogu] Bash is not interactive mode!\"
  eval \"\$(cat ~/.bashrc | tail -n +10)\"
fi

trap 'exit' SIGINT
trap 'exit' SIGTERM


while true
do
  echo -n \"[dogu] Check /mnt/.dogu_system...\"

  until [[ -n \"\$(ls /mnt/.dogu_system/ 2>/dev/null)\" ]]
  do
    sleep 1
  done

  echo \" OK\"


  echo \"[dogu] Wait for the master to start...\"

  until rosnode list > /dev/null 2>&1
  do
    sleep 1
  done

  echo \"[dogu] Master has started! ROS will launch in 10 seconds.\"


  sleep 10
  echo

  roslaunch ${HOME}/.dogu_system/bringup/${SRV_NAME}.launch &
  PID=\$!


  python -c \"\$(echo -e \"import rospy\nimport uuid\nid = 'test_'+str(uuid.uuid4()).replace('-', '_')\nrospy.init_node(id)\nrospy.set_param(id, id)\nrate = rospy.Rate(1)\nwhile not rospy.is_shutdown():\n\trospy.get_param(id)\n\trate.sleep()\n\")\" >/dev/null 2>&1


  echo \"[dogu] Disconnected from master! Shut down the ROS.\"

  kill \${PID}
  wait \${PID}

  echo
  echo \"[dogu] Done.\"
  echo \"---\"
done" |
tee ~/.dogu_system/bringup/${SRV_NAME}.sh >/dev/null


# Create launch file in dogu_system
echo -e '<launch>
  <include file="$(find dogu_es)/launch/full.launch">
  </include>

  <include file="$(find ai_event_publisher)/launch/start.launch">
  </include>

  <include file="$(find dogu_manager_agent)/launch/dogu_agent.launch">
  </include>
</launch>' > ~/.dogu_system/bringup/${SRV_NAME}.launch


# # Apply bringup service
# systemctl --user daemon-reload
# systemctl --user enable ${SRV_NAME}.service

