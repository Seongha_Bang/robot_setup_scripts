#!/bin/bash

BRINGUP_SERVICE_NAME="dogu"

DIR="$(dirname "$(realpath -s "${BASH_SOURCE}")")"
W_DIR="$(pwd)"


gitClone() {
  echo -e "[Setup] Cloning ${1} package..."

  git clone $(echo "${2}" | sed 's/gitlab.com/gitlabdoguxyz:dogong2020!!@gitlab.com/g')  # Log in to gitlab with a pull-only account

  echo -e "\n[Setup] ---Done---\n"
}


# Update pyyaml for rbs_server
pip2 install -U pyyaml


# Clone ES files
cd ~/.dogu_system

gitClone "ES" \
  "https://gitlab.com/dogong/sound/es.git"


# Create catkin workspace
mkdir ~/catkin_ws/src -p ; cd ~/catkin_ws/src


# Clone dogu's repos

gitClone "agent" \
  "https://gitlab.com/dogong/service-layer/dogu_agent.git"

gitClone "common" \
  "https://gitlab.com/dogong/ad/dogu_common.git"

gitClone "ai event publisher" \
  "https://gitlab.com/dogong/agent_ai_system_planner/d-ai/ai_event_publisher.git"

gitClone "dogu effect sound" \
  "https://gitlab.com/dogong/sound/dogu_es.git"


# Create & Enable auto script
bash ${DIR}/service.sh ${BRINGUP_SERVICE_NAME}

# Setup dogu_agent
bash ${DIR}/agent.sh


# Install dependency packages
sudo apt-get install --reinstall -y libusb-dev $(echo $(sudo apt list --installed | grep -Po "^libqt5[^\/]+"))

bash -i -c 'cd ~/catkin_ws; rosdep install --from-paths src --ignore-src --rosdistro=${ROS_DISTRO} -y'

bash ~/catkin_ws/src/dogu_es/dogu_es/setup/install.sh

if [[ ! -d /usr/include/opencv2 ]]
then
  sudo ln -s /usr/include/opencv4/opencv2 /usr/include
fi


# Build ROS packages
bash -i -c "cn"


# Copy config files of AI event publisher
mkdir ${HOME}/.dogu_system/ai
cp ${HOME}/catkin_ws/src/ai_event_publisher/config/* ${HOME}/.dogu_system/ai


cd ${W_DIR}

