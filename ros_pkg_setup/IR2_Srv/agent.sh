#!/bin/bash

MANAGER_AGENT_SRC="${HOME}/catkin_ws/src/dogu_agent/dogu_manager_agent"


# # SSHFS
# bash ${MANAGER_AGENT_SRC}/setup/sftp/setup.sh

# Install dependency
bash ${MANAGER_AGENT_SRC}/setup/setup.sh

# Build SMACH
bash -i -c "
export HISTSIZE=0
cd ~/catkin_ws
catkin_make_isolated --install --source src/dogu_agent/dogu_state_manager/thirdparty/executive_smach-noetic-devel/
catkin_make_isolated --install --source src/dogu_agent/dogu_state_manager/thirdparty/executive_smach_visualization-melodic-devel/
rosdep install smach_viewer
"

