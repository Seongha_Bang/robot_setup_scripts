#!/bin/bash

DIR="$(dirname "$(realpath -s "${BASH_SOURCE}")")"


eval "$(cat ~/.bashrc | tail -n +10)"


gitClone() {
  echo -e "[Setup] Cloning ${1} package...\n"
  git clone ${2}
  echo -e "\n[Setup] ---Done---\n\n"
}


if [[ -z $(sudo systemctl list-unit-files | grep -Po dbot) ]]
then
  bash ${DIR}/NC_dbot-start/setup.sh
fi


mkdir ~/catkin_ws/src -p ; cd ~/catkin_ws/src

gitClone "actuation" \
  "https://gitlab.com/dogong/hanhwa/dogu_actuation.git"

gitClone "agent" \
  "https://gitlab.com/dogong/service-layer/dogu_agent.git"

gitClone "new_campion" \
  "https://gitlab.com/dogong/new_campion.git"

rm -rf new_campion/dbot_p2p_nav


sudo apt-get install -y \
  ros-melodic-joy \
  libusb-dev \
  ros-melodic-geodesy \
  ros-melodic-nmea-msgs \
  ros-melodic-eigen-stl-containers \
  ros-melodic-graph-msgs \
  ros-melodic-teb-local-planner \
  ros-melodic-slam-toolbox \
  ros-melodic-move-base-msgs

sudo apt-get install --reinstall $(echo $(sudo apt list --installed | grep -Po "^libqt5[^\/]+"))

cn
