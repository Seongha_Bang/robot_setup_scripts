#!/bin/bash

DIR="$(dirname "$(realpath -s "${BASH_SOURCE}")")"


gitClone() {
  echo -e "[Setup] Cloning ${1} package...\n"
  git clone ${2}
  echo -e "\n[Setup] ---Done---\n\n"
}


if [[ -z $(sudo systemctl list-unit-files | grep -Po dbot) ]]
then
  bash ${DIR}/NC_jetson/dbot/setup.sh
fi


mkdir ~/catkin_ws/src -p ; cd ~/catkin_ws/src

gitClone "new_campion (service)" \
  "https://gitlab.com/dogong/new_campion_service.git"

gitClone "dogu_es" \
  "https://gitlab.com/dogong/sound/dogu_es.git"

cp -r ${DIR}/NC_jetson/wibotic_msg ~/catkin_ws/src
rm -rf ~/catkin_ws/src/new_campion_service/dogu_es

sudo apt-get install -y \
  libusb-dev \
  ros-melodic-eigen-stl-containers \
  ros-melodic-graph-msgs

source ~/catkin_ws/src/new_campion_service/dogu_es/dogu_es/setup/Install.sh


bash -i -c 'cn'
