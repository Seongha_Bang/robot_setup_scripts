#!/bin/bash

export HISTSIZE=0


if [[ $- != *i* ]]
then
  echo "[Warn] Bash is not interactive mode!"
  eval "$(cat ~/.bashrc | tail -n +10)"
fi

trap 'exit' SIGINT
trap 'exit' SIGTERM


while true
do
  echo "[dbot] Wait for the master to start..."

  until rosnode list > /dev/null 2>&1
  do
    sleep 1
  done

  echo "[dbot] Master has started! ROS will launch in 10 seconds."


  sleep 10
  echo

  roslaunch dbot_bringup dbot.launch &
  PID=$!


  python -c "$(echo -e "import rospy\nimport uuid\nid = 'test_'+str(uuid.uuid4()).replace('-', '_')\nrospy.init_node(id)\nrospy.set_param(id, id)\nrate = rospy.Rate(1)\nwhile not rospy.is_shutdown():\n\trospy.get_param(id)\n\trate.sleep()\n")" >/dev/null 2>&1


  echo "[dbot] Disconnected from master! Shut down the ROS."

  kill ${PID}
  wait ${PID}

  echo
  echo "[dbot] Done."
  echo "---"
done
