#!/bin/bash

DIR="$(dirname "$(realpath -s "${BASH_SOURCE}")")"


echo "[Unit]
Description=dbot start
After=network-online.target

[Service]
ExecStart=/bin/bash -i /home/dbot-start.sh
Restart=on-failure
RestartSec=10

[Install]
WantedBy=default.target" > ${DIR}/dbot.service


sudo cp ${DIR}/dbot-start.sh /home/dbot-start.sh
sudo cp ${DIR}/dbot.service  /lib/systemd/system/dbot.service

sudo systemctl daemon-reload
