#!/bin/bash

DIR="$(dirname "$(realpath -s "${BASH_SOURCE}")")"


echo "[Unit]
Description=dbot start
After=network-online.target

[Service]
User=$(whoami)
Type=simple
ExecStart=/bin/bash /home/dbot-start.sh

[Install]
WantedBy=multi-user.target" > ${DIR}/dbot.service


sudo cp ${DIR}/dbot-start.sh /home/dbot-start.sh
sudo cp ${DIR}/dbot.service  /lib/systemd/system/dbot.service

sudo systemctl daemon-reload
sudo systemctl enable dbot.service
