#!/bin/bash

BRINGUP_SERVICE_NAME="dogu"

DIR="$(dirname "$(realpath -s "${BASH_SOURCE}")")"
W_DIR="$(pwd)"


gitClone() {
  echo -e "[Setup] Cloning ${1} package..."

  git clone $(echo "${2}" | sed 's/gitlab.com/gitlabdoguxyz:dogong2020!!@gitlab.com/g')  # Log in to gitlab with a pull-only account

  echo -e "\n[Setup] ---Done---\n"
}


# Create catkin workspace
mkdir ~/catkin_ws/src -p ; cd ~/catkin_ws/src


# Clone dogu's repos

gitClone "agent" \
  "https://gitlab.com/dogong/service-layer/dogu_agent.git"

gitClone "actuation" \
  "https://gitlab.com/dogong/ad/dogu_actuation.git"

gitClone "autonomous" \
  "https://gitlab.com/dogong/ad/dogu_autonomous.git"

gitClone "car tracker" \
  "https://gitlab.com/dogong/ad/dogu_car_tracker.git"

gitClone "common" \
  "https://gitlab.com/dogong/ad/dogu_common.git"

gitClone "docking" \
  "https://gitlab.com/dogong/ad/dogu_docking.git"

gitClone "driver" \
  "https://gitlab.com/dogong/ad/dogu_driver.git"


# Setup udev rule
bash ${DIR}/udev.sh

# Create & Enable auto script
bash ${DIR}/service.sh ${BRINGUP_SERVICE_NAME}

# Setup dogu_agent
bash ${DIR}/agent.sh


# Install dependency packages
sudo apt-get install --reinstall -y libusb-dev $(echo $(sudo apt list --installed | grep -Po "^libqt5[^\/]+"))
sudo apt-get install -y ros-melodic-jsk-recognition-msgs ros-melodic-vision-msgs ros-melodic-geodesy ros-melodic-nmea-msgs ros-melodic-grid-map-ros ros-melodic-jsk-rviz-plugins ros-melodic-geodesy ros-melodic-pcl-ros ros-melodic-nmea-msgs ros-melodic-libg2o

sudo wget -qO- https://raw.githubusercontent.com/luxonis/depthai-ros/main/install_dependencies.sh | sudo bash

bash -i -c 'cd ~/catkin_ws; rosdep install --from-paths src --ignore-src --rosdistro=${ROS_DISTRO} -y'


# Build ROS packages
bash -i -c "cn"


cd ${W_DIR}

