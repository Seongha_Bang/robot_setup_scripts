#!/bin/bash

DIR="$(dirname "$(realpath -s "${BASH_SOURCE}")")"

if [[ -n "${1}" ]]
then
  SRV_NAME="${1}"
else
  SRV_NAME="dogu"
fi


mkdir ~/.dogu_system/bringup

# Create bringup service file
echo "[Unit]
Description=${SRV_NAME} start
After=network-online.target

[Service]
User=${USER}
Type=simple
ExecStart=/bin/bash -i ${HOME}/.dogu_system/bringup/${SRV_NAME}.sh
Restart=on-failure
RestartSec=10

[Install]
WantedBy=multi-user.target" |
sudo tee /etc/systemd/system/${SRV_NAME}.service >/dev/null

# Create bringup script file
echo '#!/bin/bash' "

export HISTSIZE=0
unset HISTFILE


if [[ $- != *i* ]]
then
  echo \"[Warn] Bash is not interactive mode!\"
  eval \"\$(cat ~/.bashrc | tail -n +10)\"
fi


roslaunch ${HOME}/.dogu_system/bringup/${SRV_NAME}.launch" |
tee ~/.dogu_system/bringup/${SRV_NAME}.sh >/dev/null


# Create launch file in dogu_system
echo -e '<launch>
  <master auto="start"/>

  <include file="$(find dogu_bringup)/launch/bringup.launch">
  </include>
</launch>' > ~/.dogu_system/bringup/${SRV_NAME}.launch


# Apply bringup service
sudo systemctl daemon-reload
sudo systemctl enable ${SRV_NAME}.service

