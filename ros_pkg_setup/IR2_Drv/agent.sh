#!/bin/bash

MANAGER_AGENT_SRC="${HOME}/catkin_ws/src/dogu_agent/dogu_manager_agent"

W_DIR="$(pwd)"


# SSH
sudo apt install -y openssh-server
sudo ufw allow ssh


# Copy config files to dogu_system
cp ${MANAGER_AGENT_SRC}/config/robot.yaml ~/.dogu_system
cp -r ${MANAGER_AGENT_SRC}/config/module_manager ~/.dogu_system
cp ${MANAGER_AGENT_SRC}/config/module_params_*.yaml ~/.dogu_system/module_manager


# Add executor service
echo '#!/bin/bash' "

export HISTSIZE=0

sleep 45
roslaunch dogu_module_executor dogu_executor.launch" |
tee ~/.dogu_system/bringup/executor-start.sh >/dev/null
echo "[Unit]
Description=Start Dogu Executor
After=network-online.target

[Service]
User=${USER}
Type=simple
ExecStart=/bin/bash -i ${HOME}/.dogu_system/bringup/executor-start.sh

[Install]
WantedBy=multi-user.target" |
sudo tee /etc/systemd/system/dogu-executor.service >/dev/null

sudo systemctl enable dogu-executor


# Setup mysql
sudo apt-get install -y mysql-server
sudo ufw allow mysql

sudo systemctl start mysql
sudo systemctl enable mysql

sudo /usr/bin/mysql -u root --password="" --execute="
# DOGU_REGISTER 데이터 베이스 만들기
CREATE DATABASE DOGU_REGISTER;

# DOGU_REGISTER 데이터베이스를 사용할 계정을 만들기
CREATE USER 'dogu'@'localhost' IDENTIFIED BY '0000';
FLUSH PRIVILEGES;

# DOGU_REGISTER 데이터베이스를 사용할 계정에 권한 부여
GRANT ALL PRIVILEGES ON DOGU_REGISTER.* TO 'dogu'@'localhost';
FLUSH PRIVILEGES;
"

# Setup strapi
cd ~
sudo apt-get install -y build-essential
sudo npm install strapi@beta -g

zenity --info --title="strapi setup flow" --text="[Selection order]\n- Custom\n- javascript\n- mysql\n- DOGU_REGISTER\n- 127.0.0.1\n- 3306\n- username: dogu\n- password: 0000\n- y" --ellipsize 2>/dev/null & disown
npx create-strapi-app doguRegister

# echo -e "\n---Done---\n\n  Please run following command\n┏ and continue with installation ┓\n\n  cd ~/doguRegister\n  npm run develop\n\n┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛"
# sleep 5
zenity --info --title="strapi auto setup done" --text="Please run following command\nand continue with installation.\n\ncd ~/doguRegister\nnpm run develop" --ellipsize 2>/dev/null & disown


cd ${W_DIR}

