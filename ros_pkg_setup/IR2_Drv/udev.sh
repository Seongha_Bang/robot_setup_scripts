#!/bin/bash

echo '# serial device setting
SUBSYSTEM=="usb", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6015", MODE:="0777"
# MRP-2000(microUSB) setting
KERNEL=="ttyACM*", ATTRS{idVendor}=="1546", ATTRS{idProduct}=="01a9", MODE:="0777", SYMLINK+="mrp2000"
# U-blox(C94-M8P-3) setting
KERNEL=="ttyACM*", ATTRS{idVendor}=="1546", ATTRS{idProduct}=="01a8", MODE:="0777", SYMLINK+="ublox"
# rplidar setting
KERNEL=="ttyUSB*", ATTRS{idVendor}=="10c4", ATTRS{idProduct}=="ea60", DEVPATH=="/devices/pci0000:00/0000:00:14.0/usb1/1-5/1-5:1.0/ttyUSB*/tty/ttyUSB*", MODE:="0777", SYMLINK+="rplidar"
# e2box setting
KERNEL=="ttyUSB*", ATTRS{idVendor}=="10c4", ATTRS{idProduct}=="ea60", DEVPATH=="/devices/pci0000:00/0000:00:14.0/usb1/1-9/1-9:1.0/ttyUSB*/tty/ttyUSB*", MODE:="0777", SYMLINK+="e2box"
# imu setting
KERNEL=="ttyUSB*", ATTRS{idVendor}=="2639", ATTRS{idProduct}=="0003", MODE:="0777", SYMLINK+="xsens"
# GL3 setting
SUBSYSTEM=="usb", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6014", MODE:="0777", SYMLINK+="GL3"
# rear cam setting
KERNEL=="video*", ATTR{index}=="0", ATTRS{idVendor}=="15aa", ATTRS{idProduct}=="1555", MODE:="0777", SYMLINK+="cam_rear"' | sudo tee /etc/udev/rules.d/dogu.rules

sudo bash -c 'udevadm control --reload; udevadm trigger'

