#!/bin/bash

DIR="$(dirname "$(realpath -s "${BASH_SOURCE}")")"
TARGET_DESKTOP_FILE="${HOME}/.config/autostart/execute-setup-script.desktop"


sudo apt update
sudo apt upgrade -y

# Install GUI
echo -e "[ Install GUI ]"

sudo apt install -y gdm3 ubuntu-desktop pavucontrol

echo -e "\n--- Done --- \n"

# Execute sub setup script after reboot
mkdir -p $(dirname ${TARGET_DESKTOP_FILE})
echo "[Desktop Entry]

Encoding=UTF-8
Version=1.0
Type=Application
Terminal=true

Exec=/bin/bash -c 'bash ~/.dogu_system/robot_setup_scripts/setup_vim3-2nd.sh; rm ${TARGET_DESKTOP_FILE}; echo '[ Setup is done ]'; while true; do sleep 3600; done'
" | tee ${TARGET_DESKTOP_FILE} >/dev/null

read -p '(Press enter to reboot)' line && sudo reboot
