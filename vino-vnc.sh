#! /bin/bash


# Setup virtual display
echo -e "[ Setup virtual display ]\n"

if [[ -z "$(grep Virtual /etc/X11/xorg.conf)" ]]
then
  virtual_scr='
Section "Screen"
   Identifier    "Default Screen"
   Monitor       "Configured Monitor"
   Device        "Tegra0"
   SubSection "Display"
       Depth    24
       Virtual 1920 1080
   EndSubSection
EndSection'

  echo -e "${virtual_scr}" | sudo tee -a /etc/X11/xorg.conf > /dev/null
else
  echo "Virtual screen is already set"
fi

echo -e "\n--- Done --- \n\n"


# Enable remote access
echo -e "[ Enable remote access ]\n"

if [[ -z "$(grep 'Enable remote access' /usr/share/glib-2.0/schemas/org.gnome.Vino.gschema.xml)" ]]
then
  sudo sed -i "$(( $(grep '</key>' /usr/share/glib-2.0/schemas/org.gnome.Vino.gschema.xml -n | cut -d':' -f1 | tail -n 1) + 1 ))i\ \n    <key name='enabled' type='b'>\n     <summary>Enable remote access to the desktop</summary>\n     <description>\n       If true, allows remote access to the desktop via the RFB\n       protocol. Users on remote machines may then connect to the\n       desktop using a VNC viewer.\n     </description>\n     <default>false</default>\n   </key>" /usr/share/glib-2.0/schemas/org.gnome.Vino.gschema.xml
fi

echo -e "\n--- Done --- \n\n"


# Enable vino-server service
echo "[Unit]
Description=Vino VNC server
After=network-online.target

[Service]
ExecStart=/usr/lib/vino/vino-server --display=:0
Restart=on-failure
RestartSec=5s
StandardOutput=null

[Install]
WantedBy=default.target" | sudo tee /usr/lib/systemd/user/vino-server-auto.service >/dev/null
systemctl --user enable vino-server-auto.service


# Setting vino-server
echo -e "[ Setting vino-server ]\n"

gsettings set org.gnome.Vino prompt-enabled false
gsettings set org.gnome.Vino require-encryption false
sudo glib-compile-schemas /usr/share/glib-2.0/schemas
gsettings set org.gnome.Vino authentication-methods "['vnc']"

if [[ "$(gsettings get org.gnome.Vino vnc-password)" == "'keyring'" ]]
then
  gsettings set org.gnome.Vino vnc-password $(echo -n '0000'|base64)
fi

if [[ -n "$1" ]]
then
  gsettings set org.gnome.Vino alternative-port $1
  gsettings set org.gnome.Vino use-alternative-port true
fi

# Start vino-server
systemctl --user start vino-server-auto.service

echo -e "\n--- Done --- \n\n"

