#! /bin/bash

clients="${1}"
src="${2}"


if [[ -z "${clients}" ]]
then
  echo "<Usage>"
  echo "${0} {clients} [target source (pulse)]"
  echo
  echo "(Example: ${0} 192.168.42.100:5000,192.168.42.101:5001)"

  exit
fi

if [[ -n "${src}" ]]
then
  src_line="device=${src}"
fi


echo "[Unit]
Description=Dogu Sound Sharing (Publisher)
After=network-online.target

[Service]
ExecStart=/bin/bash -c \"gst-launch-1.0 pulsesrc ${src_line} ! audioresample ! audio/x-raw, rate=48000, format=F32LE ! audioconvert ! multiudpsink clients=${clients}\"
Restart=on-failure
RestartSec=5s

[Install]
WantedBy=default.target" | sudo tee /etc/systemd/user/dogu-sound-sharing.service

systemctl --user enable dogu-sound-sharing.service
