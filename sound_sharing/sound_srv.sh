#! /bin/bash


echo "[Unit]
Description=Dogu Sound Sharing
After=network-online.target

[Service]
ExecStart=/bin/bash -c \"until pulseaudio --check; do sleep 1; done; while true; do gst-launch-1.0 udpsrc multicast-group=192.168.42.78 port=6401 caps='audio/x-raw, channels=(int)2, rate=(int)48000, width=(int)16, depth=(int)16' ! audioconvert ! 'audio/x-raw, rate=(int)48000, format=(string)S16LE, channels=(int)2, layout=(string)interleaved, channel-mask=(bitmask)0x0000000000000003' ! pulsesink device=alsa_output.front_card.analog-stereo; sleep 1; done& while true; do gst-launch-1.0 pulsesrc device=alsa_input.front_card.analog-mono ! audioresample ! audio/x-raw, rate=48000, format=S16LE ! audioconvert ! multiudpsink clients=192.168.42.88:6402; sleep 1; done\"
Restart=on-failure
RestartSec=5s

[Install]
WantedBy=default.target" | sudo tee /etc/systemd/user/dogu-sound-sharing.service

systemctl --user enable dogu-sound-sharing.service
