#! /bin/bash

host=${1}
port=${2}
sink=${3}


if [[ -z "${host}" || -z "${port}" ]]
then
  echo "<Usage>"
  echo "${0} {IP (self)} {port} [target sink (pulse)]"

  exit
fi

if [[ -n "${sink}" ]]
then
  sink_line="device=${sink}"
fi


echo "[Unit]
Description=Dogu Sound Sharing (Subscriber)
After=network-online.target

[Service]
ExecStart=/bin/bash -c \"gst-launch-1.0 udpsrc multicast-group=${host} port=${port} caps='audio/x-raw, channels=(int)2, rate=(int)48000, width=(int)16, depth=(int)16' ! audioconvert ! 'audio/x-raw, rate=(int)48000, format=(string)F32LE, channels=(int)2, layout=(string)interleaved, channel-mask=(bitmask)0x0000000000000003' ! pulsesink ${sink_line}\"
Restart=on-failure
RestartSec=5s

[Install]
WantedBy=default.target" | sudo tee /etc/systemd/user/dogu-sound-sharing.service

systemctl --user enable dogu-sound-sharing.service
