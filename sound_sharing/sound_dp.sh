#! /bin/bash


echo "[Unit]
Description=Dogu Sound Sharing
After=network-online.target

[Service]
ExecStart=/bin/bash -c \"until pulseaudio --check; do sleep 1; done; while true; do gst-launch-1.0 pulsesrc device=speaker.monitor ! audioresample ! audio/x-raw, rate=48000, format=S16LE ! audioconvert ! multiudpsink clients=192.168.42.78:6401; sleep 1; done& while true; do gst-launch-1.0 udpsrc multicast-group=192.168.42.88 port=6402 caps='audio/x-raw, channels=(int)2, rate=(int)48000, width=(int)16, depth=(int)16' ! audioconvert ! 'audio/x-raw, rate=(int)48000, format=(string)S16LE, channels=(int)2, layout=(string)interleaved, channel-mask=(bitmask)0x0000000000000003' ! pulsesink device=microphone_body; sleep 1; done\"
Restart=on-failure
RestartSec=5s

[Install]
WantedBy=default.target" | sudo tee /etc/systemd/user/dogu-sound-sharing.service

echo "---"

echo "# Create null-sink for streaming

load-module module-null-sink sink_name=speaker
update-sink-proplist   speaker device.description=speaker
update-source-proplist speaker.monitor device.description=speaker.monitor

load-module module-null-sink sink_name=microphone_body
update-sink-proplist microphone_body device.description=microphone_body
update-source-proplist microphone_body.monitor device.description=microphone_body.monitor

load-module module-virtual-source source_name=microphone master=microphone_body.monitor
update-source-proplist microphone device.description=microphone

set-default-sink speaker
set-default-source microphone" | sudo tee /etc/pulse/default.pa.d/dogu-null-sink.pa

systemctl --user enable dogu-sound-sharing.service
